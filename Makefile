inkscape:=$(shell which inkscape)

ifeq ($(inkscape),)
	inkscape=/Applications/Inkscape.app/Contents/Resources/bin/inkscape
endif

# using homebrew python
# python:=/usr/local/bin/python2
python:=python

fig_7_tp = T05

all: figure1 figure2 figure3 figure4 figure5 figure6 figure7 figureS1 figureS2 figureS3 figureS7 figureS9 paper

figure1:
	${inkscape} -D -A `pwd`/paper/figures/figure1.pdf `pwd`/paper/figures/figure1.svg

figure2:
	${python} ./scripts/neighbour_heatmap.py 3002_PD T00 --limits [3,9] --outname figure2a_1.png
	${python} ./scripts/neighbour_heatmap.py 3002_PD T09 --limits [3,9] --outname figure2a_2.png
	${python} ./scripts/divs_from_data.py 3002_PD T05 T06 -s True -o figure2c.png
	${python} ./scripts/write_extracted_data.py 
	${inkscape} -D -A `pwd`/paper/figures/figure2.pdf `pwd`/paper/figures/figure2.svg

figure3:
	${python} ./scripts/pool_data.py
	${python} ./scripts/fig_compare_youngold.py 3002_PD T02 T09 -o ../paper/figures
	${python} ./scripts/fig_conserved_topo.py 3002_PD T06 -o ../paper/figures
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d extracted_data
	${inkscape} -D -A `pwd`/paper/figures/figure3.pdf `pwd`/paper/figures/figure3.svg

figure4:
	${inkscape} -D -A `pwd`/paper/figures/figure4.pdf `pwd`/paper/figures/figure4.svg

figure5:
	${python} ./scripts/network_model.py -t synchronous -m pascal -o ../output/model_results/pascal_sync.json
	${python} ./scripts/network_model.py -t synchronous -m 3sided -o ../output/model_results/random_sync.json
	${python} ./scripts/network_model.py -t synchronous -m equal_div -o ../output/model_results/equal_div_sync.json
	${python} ./scripts/network_model.py -t asynchronous -m equal_div -o ../output/model_results/equal_div_async.json
	${python} ./scripts/pool_data.py
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d model_comp
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d picking_behaviour
	${inkscape} -D -A `pwd`/paper/figures/figure5.pdf `pwd`/paper/figures/figure5.svg

figure6:
	${inkscape} -D -A `pwd`/paper/figures/figure6.pdf `pwd`/paper/figures/figure6.svg

figure7:
	${python} ./scripts/aboav_weire.py 3002_PD ${fig_7_tp} --outname figure7
	${python} ./scripts/area_heatmap.py 3002_PD ${fig_7_tp} --outname figure7b.png
	${python} ./scripts/anisotropy_heatmap.py 3002_PD ${fig_7_tp} -o figure7d.png --colormap viridis --range [1,3]
	${inkscape} -D -A `pwd`/paper/figures/figure7.pdf `pwd`/paper/figures/figure7.svg

figureS1:
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d comp_jonsson_all
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d comp_jonsson
	${inkscape} -D -A `pwd`/paper/figures/figureS1.pdf `pwd`/paper/figures/figureS1.svg

figureS2:
	${python} ./scripts/area_heatmap_with_dist.py 3002_PD T02 T09 --outname figureS2a.png
	${python} ./scripts/fig_conserved_topo_area_dists.py 3002_PD T06 -o ../paper/figures
	${inkscape} -D -A `pwd`/paper/figures/figureS2.pdf `pwd`/paper/figures/figureS2.svg

figureS3:
	${python} ./scripts/network_model.py -t synchronous -m pascal -o2 ../output/model_results/pascal_division_neigh.json
	${python} ./scripts/network_model.py -t synchronous -m 3sided -o2 ../output/model_results/random_division_neigh.json
	${python} ./scripts/network_model.py -t synchronous -m equal_div -o2 ../output/model_results/equal_division_neigh.json
	${python} ./scripts/aboav_weire_from_model.py ../output/model_results/pascal_division_neigh.json -o figureS3a.png
	${python} ./scripts/aboav_weire_from_model.py ../output/model_results/random_division_neigh.json -o figureS3b.png
	${python} ./scripts/aboav_weire_from_model.py ../output/model_results/equal_division_neigh.json -o figureS3c.png
	${inkscape} -D -A `pwd`/paper/figures/figureS3.pdf `pwd`/paper/figures/figureS3.svg

figureS7:
	${python} ./scripts/check_dividing_areas.py

figureS9:
	${python} ./scripts/neighbour_heatmap.py 2823_PA T00 --limits [0,12] --outname figureS9a.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T01 --limits [0,12] --outname figureS9b.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T03 --limits [0,12] --outname figureS9c.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T04 --limits [0,12] --outname figureS9d.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T05 --limits [0,12] --outname figureS9e.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T06 --limits [0,12] --outname figureS9f.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T07 --limits [0,12] --outname figureS9g.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T08 --limits [0,12] --outname figureS9h.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T09 --limits [0,12] --outname figureS9i.png
	${python} ./scripts/neighbour_heatmap.py 2823_PA T10 --limits [0,12] --outname figureS9j.png
	convert -transparent white paper/figures/figureS9a.png paper/figures/figureS9a.png
	convert -transparent white paper/figures/figureS9b.png paper/figures/figureS9b.png
	convert -transparent white paper/figures/figureS9c.png paper/figures/figureS9c.png
	convert -transparent white paper/figures/figureS9d.png paper/figures/figureS9d.png
	convert -transparent white paper/figures/figureS9e.png paper/figures/figureS9e.png
	convert -transparent white paper/figures/figureS9f.png paper/figures/figureS9f.png
	convert -transparent white paper/figures/figureS9g.png paper/figures/figureS9g.png
	convert -transparent white paper/figures/figureS9h.png paper/figures/figureS9h.png
	convert -transparent white paper/figures/figureS9i.png paper/figures/figureS9i.png
	convert -transparent white paper/figures/figureS9j.png paper/figures/figureS9j.png
	${python} ./scripts/plot_distributions.py -o ../paper/figures -d WT_comparison
	${inkscape} -D -A `pwd`/paper/figures/figureS9.pdf `pwd`/paper/figures/figureS9.svg

paper: FORCE
	$(MAKE) -C ./paper paper

FORCE: ;

dropbox:
	git ls-files -o > output/list_of_files_not_part_of_git_copied_to_Dropbox
	rsync -avz --files-from=output/list_of_files_not_part_of_git_copied_to_Dropbox . ~/Dropbox/PCTopology
