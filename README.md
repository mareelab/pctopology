# PCTopology

## Investigation into *Arabidopsis* leaf epidermis topology.

`./data/raw` contains the raw data in the form of both raw and segmented images

`make all` will analyse all data, generate all figures and place them in the folder output/figures

## This work has been published:

**Pavement cells and the topology puzzle**

Ross Carter<sup>1,3</sup>, Yara E. Sánchez-Corrales<sup>1,2,3</sup>, Matthew Hartley<sup>1</sup>, 
Verônica A. Grieneisen<sup>1,4</sup> and Athanasius F. M. (Stan) Marée<sup>1,4</sup>

*Development* (2017), v. 144, pp. 4386-4397, [doi:10.1242/dev.157073](http://dx.doi.org/10.1242/dev.157073)

1. Computational & Systems Biology, John Innes Centre, Norwich, UK
2. Current address: MRC-Laboratory of Molecular Biology, Cambridge Biomedical Campus, Cambridge, UK
3. These authors contributed equally to this work.
4. To whom correspondence should be addressed. Email: *Veronica.Grieneisen@jic.ac.uk*, *Stan.Maree@jic.ac.uk*.