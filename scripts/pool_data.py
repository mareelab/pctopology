"""Pools the neighbourhood distributions for all leaves"""
import os
import csv
import json
import numpy as np

import common_functions as ttf


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'

    data_array = np.zeros(shape=[1, 201])
    tot_leaves = 0
    tot_cells = 0

    dirs = os.listdir(data_dir)

    # ignore exp id 2823 as this is col0!!!

    for exp_dir in dirs[2:]:
        if exp_dir[0] is not '.':
            for json_file in os.listdir(os.path.join(data_dir, exp_dir)):
                if 'neighbours.json' in json_file:
                    tot_leaves += 1
                    print exp_dir, json_file[0:3]
                    hist, un_norm_hist, cells = get_hist(exp_dir, json_file[0:3])
                    hist_array = np.asarray(hist)
                    data_array = np.vstack((data_array, hist_array))
                    tot_cells += cells

    data_array = data_array[1:]
    data_array = np.transpose(data_array)

    total_hist_dict = {}

    for i, row in enumerate(data_array[1:]):
        total_hist_dict[str(i)] = np.sum(row.astype('float')/len(row))
    json_out = '../output/arabidopsis_distributions.json'
    with open(json_out, 'w') as json_out_handle:
        json.dump(total_hist_dict, json_out_handle)

    out_path = '../output/pooled_arabidopsis_data.csv'
    with open(out_path, 'wb') as out_handle:
        writer = csv.writer(out_handle, delimiter=',')
        for row in data_array:
            writer.writerow(row)

    print 'total leaves: %s' % tot_leaves
    print 'total cells: %s' % tot_cells


def get_hist(exp_id, time_point):
    # os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    json_path = os.path.join(data_dir, exp_id, (time_point + '_neighbours.json'))

    neighbours_dict = ttf.load_neighbours_dictionary_json(json_path)
    non_edge_neighbours_dict = {k: v for k, v in neighbours_dict.iteritems() if 0 not in v['neighbours']}
    if 0 in non_edge_neighbours_dict.keys():
        del(non_edge_neighbours_dict[0])

    """neighbour counts and histograms"""
    num_neighbours_list = []
    for cid, data_dict in non_edge_neighbours_dict.iteritems():
        num_neighbours_list.append(data_dict['num_neighbours'])

    name_string = "%s_%s" % (exp_id, time_point)

    num_neighbours_hist = [0] * 200
    for i in num_neighbours_list:
        num_neighbours_hist[i] += 1

    tot_cells = sum(num_neighbours_hist)

    try:
        num_neighbours_hist_norm = [float(x) / sum(num_neighbours_hist) for x in num_neighbours_hist]
        num_neighbours_hist_norm.insert(0, name_string)

    except ZeroDivisionError:
        num_neighbours_hist_norm = [0] * 200
        num_neighbours_hist_norm.insert(0, name_string)

    return num_neighbours_hist_norm, num_neighbours_hist, tot_cells


if __name__ == '__main__':
    main()
