"""Test Aboav Weire for a time point"""
import os
import argparse
import json
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    output_dir = '../paper/figures'

    """average number of neighbours' neighbours"""

    with open(args.data_path, 'r') as fh:
        data_dict = json.load(fh)

    # data_dict = {"1": ["2", "3"],
    #              "2": ["1", "3"],
    #              "3": ["1", "2"]}

    av_neigh_neighbours = {}
    for cid, n_list in data_dict.iteritems():
        # print "="*20
        nnlist = []
        for neighbour in n_list:
            nnlist.append(len(data_dict[neighbour]))
            # print len(data_dict[neighbour])
        av_neigh_neighbours[cid] = np.mean(nnlist)
        # print len(n_list), np.mean(nnlist)

    dict_by_nneighbours = defaultdict(list)
    neighbours = []
    for k, v in av_neigh_neighbours.iteritems():
        dict_by_nneighbours[len(data_dict[k])].append(v)
        neighbours.append(v)

    x1 = []
    y1 = []
    y1_sd = []
    for k, v in dict_by_nneighbours.iteritems():
        x1.append(k)
        y1.append(np.mean(v))
        y1_sd.append(np.std(v))

    x2 = range(0, 16)

    # mu_2 = [i**2 for i in y1_sd]
    # mu_2[len(x2):] = [0]*(len(mu_2)-len(x2))

    # mu_2 = np.var(neighbours)
    mu_2 = 2

    # mu_2 = [2] * len(x2)
    aw_law = [5 + (6 + mu_2) / float(i + 0.0000000001) for i in x2]

    title_string = os.path.basename(args.data_path).split('.')[0].split('_')[0:-1]
    title_string = title_string[0] + " " + title_string[1]

    font_size = 12

    plt.figure()
    plt.plot(x1, y1, 'o', label='Data', color='blue')
    plt.plot(x2, aw_law, '-', label='Aboav-Weaire Law', color='green')
    plt.errorbar(x1, y1, yerr=y1_sd, fmt='.', color='blue')
    plt.ylim([5, 12])
    plt.xlim([2, 15])
    plt.legend(loc=1, fontsize=font_size)
    plt.title(title_string.title())
    plt.xlabel('Number of Neighbours', fontsize=font_size)
    plt.ylabel('Average Number of Neighbours\' Neighbours', fontsize=font_size)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname), format='png', dpi=600)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("data_path", help="path to the model results .json file")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    main()
