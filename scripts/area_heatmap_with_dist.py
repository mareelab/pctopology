#!/usr/bin/python
"""plots a heatmap of areas and distribution of areas"""
import argparse
import os

import matplotlib
import matplotlib.colors
import matplotlib.cm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

import common_functions as ttf


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def main(args):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    json_path1 = os.path.join(data_dir, args.exp_id, (args.time_point1 + '_neighbours.json'))
    segmented_image_path1 = os.path.join(data_dir, args.exp_id, (args.time_point1 + '.png'))

    json_path2 = os.path.join(data_dir, args.exp_id, (args.time_point2 + '_neighbours.json'))
    segmented_image_path2 = os.path.join(data_dir, args.exp_id, (args.time_point2 + '.png'))

    md_dict1 = False
    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        metadata_path = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (args.time_point1 + '.txt'))
        md_dict1 = ttf.load_metadata(metadata_path)

    md_dict2 = False
    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        metadata_path = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (args.time_point2 + '.txt'))
        md_dict2 = ttf.load_metadata(metadata_path)

    cid_array1 = ttf.path2id_array(segmented_image_path1)
    neighbourhood_dictionary1 = ttf.load_neighbours_dictionary_json(json_path1)
    del neighbourhood_dictionary1[0]

    cid_array2 = ttf.path2id_array(segmented_image_path2)
    neighbourhood_dictionary2 = ttf.load_neighbours_dictionary_json(json_path2)
    del neighbourhood_dictionary2[0]

    area_list1 = []
    cids1 = []
    area_list2 = []
    cids2 = []

    for cid, v1 in neighbourhood_dictionary1.iteritems():
        if md_dict1:
            area_list1.append(v1['area'] * md_dict1['vox_x'] * md_dict1['vox_y'])
        else:
            area_list1.append(v1['area'])
        if 0 not in v1['neighbours']:
            cids1.append(cid)

    for cid, v1 in neighbourhood_dictionary2.iteritems():
        if md_dict2:
            area_list2.append(v1['area'] * md_dict2['vox_x'] * md_dict2['vox_y'])
        else:
            area_list2.append(v1['area'])
        if 0 not in v1['neighbours']:
            cids2.append(cid)

    heatmap_shape1 = [cid_array1.shape[0], cid_array1.shape[1], 4]
    heatmap_shape2 = [cid_array2.shape[0], cid_array2.shape[1], 4]

    area_heatmap1 = np.full(shape=heatmap_shape1, fill_value=[256, 256, 256, 1], dtype='float32')
    area_heatmap2 = np.full(shape=heatmap_shape2, fill_value=[256, 256, 256, 1], dtype='float32')

    area_min1 = min(area_list1)
    area_max1 = max(area_list1)
    area_min2 = min(area_list2)
    area_max2 = max(area_list2)

    color_scheme_1 = 'viridis'

    color_map_area1 = matplotlib.cm.ScalarMappable(cmap=color_scheme_1)
    color_map_area1.set_clim(vmin=area_min1, vmax=area_max1)
    color_map_area2 = matplotlib.cm.ScalarMappable(cmap=color_scheme_1)
    color_map_area2.set_clim(vmin=area_min2, vmax=area_max2)

    for cid, data in neighbourhood_dictionary1.iteritems():
        if md_dict1:
            area_heatmap1[cid_array1 == cid] = color_map_area1.to_rgba(
                data['area'] * md_dict1['vox_x'] * md_dict1['vox_y'])
        else:
            area_heatmap1[cid_array1 == cid] = color_map_area1.to_rgba(data['area'])
    for cid, data in neighbourhood_dictionary2.iteritems():
        if md_dict2:
            area_heatmap2[cid_array2 == cid] = color_map_area2.to_rgba(
                data['area'] * md_dict2['vox_x'] * md_dict2['vox_y'])
        else:
            area_heatmap2[cid_array2 == cid] = color_map_area2.to_rgba(data['area'])

    cell_outlines1 = ttf.generate_cell_outline_array(cid_array1)
    cell_outlines2 = ttf.generate_cell_outline_array(cid_array2)

    # path = segmented_image_path1.split('/')
    # title_string = path[-2] + " " + path[-1][:-4]

    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        extent1 = [0, area_heatmap1.shape[1] * md_dict1['vox_y'], 0, area_heatmap1.shape[0] * md_dict1['vox_x']]
    else:
        extent1 = [0, area_heatmap1.shape[1], 0, area_heatmap1.shape[0]]

    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        extent2 = [0, area_heatmap2.shape[1] * md_dict2['vox_y'], 0, area_heatmap2.shape[0] * md_dict2['vox_x']]
    else:
        extent2 = [0, area_heatmap2.shape[1], 0, area_heatmap2.shape[0]]

    plt.figure(figsize=(15, 7), facecolor='white')
    ax1 = plt.subplot(1, 3, 1)
    plt.imshow(area_heatmap1, cmap=color_scheme_1, interpolation='nearest', extent=extent1)
    plt.imshow(cell_outlines1, cmap=color_scheme_1, interpolation='nearest', extent=extent1)
    plt.clim(area_min1, area_max1)
    plt.xlabel(r'x, $\mu$m')
    plt.ylabel(r'y, $\mu$m')
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="5%", pad=0.05)
    cbar1 = plt.colorbar(cax=cax1)
    cbar1.set_label(r'Cell Area, $\mu$m$^{2}$', fontsize=12)
    ttf.add_scale_bar(ax1)

    ax3 = plt.subplot(1, 3, 2)
    plt.imshow(area_heatmap2, cmap=color_scheme_1, interpolation='nearest', extent=extent2)
    plt.imshow(cell_outlines2, cmap=color_scheme_1, interpolation='nearest', extent=extent2)
    plt.clim(area_min2, area_max2)
    plt.xlabel(r'x, $\mu$m')
    plt.ylabel(r'y, $\mu$m')
    divider1 = make_axes_locatable(ax3)
    cax2 = divider1.append_axes("right", size="5%", pad=0.05)
    cbar2 = plt.colorbar(cax=cax2)
    cbar2.set_label(r'Cell Area, $\mu$m$^{2}$', fontsize=12)
    ttf.add_scale_bar(ax3)

    ax2 = plt.subplot(1, 3, 3)
    mean1 = np.mean(area_list1)
    mean2 = np.mean(area_list2)
    sd1 = np.std(area_list1)
    sd2 = np.std(area_list2)
    print "Leaf A mean:" + str(mean1) + " StDev: " + str(sd1)
    print "Leaf B mean:" + str(mean2) + " StDev: " + str(sd2)
    plt.hist([area_list2, area_list1], bins=50, histtype='step',
             label=['Leaf from B', 'Leaf from A'],
             color=['blue', 'green'])
    string1 = "Leaf A, " + str.format('{0:.2f}', mean1) + " +/- " + str.format('{0:.2f}', sd1) + r" $\mu$m$^{2}$"
    string2 = "Leaf B, " + str.format('{0:.2f}', mean2) + " +/- " + str.format('{0:.2f}', sd2) + r" $\mu$m$^{2}$"
    ax2.text(0.95, 0.8, string1,
             verticalalignment='bottom', horizontalalignment='right',
             transform=ax2.transAxes,
             color='green', fontsize=12)
    ax2.text(0.95, 0.7, string2,
             verticalalignment='bottom', horizontalalignment='right',
             transform=ax2.transAxes,
             color='blue', fontsize=12)

    plt.legend()

    plt.xlabel(r'Cell Area, $\mu$m$^2$', fontsize=12)
    plt.ylabel(r'Number', fontsize=12)

    plt.subplots_adjust(left=0.04, bottom=0.07, right=0.96, top=0.94, wspace=0.4, hspace=0.2)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname), format='png', dpi=600)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point1", help="Time point for first leaf. eg. 'T00', 'T01' etc")
    parser.add_argument("time_point2", help="Time point for second leaf. eg. 'T00', 'T01' etc")
    parser.add_argument("-n", "--limits", help="colorbar_limits i.e. [0,12]")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    main(args)
