"""Pools the division data for all time points"""
import os
import copy
import numpy as np

import common_functions as cf


def analyse_timepoint(exp_id, tp1, tp2):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    n_path_1 = os.path.join(data_dir, exp_id, (tp1 + '_neighbours.json'))
    n_path_2 = os.path.join(data_dir, exp_id, (tp2 + '_neighbours.json'))

    m_path = os.path.join(data_dir, exp_id, 'cell_matches', (tp1 + tp2 + '.match'))
    div_mat = np.zeros([12, 12], dtype=float)

    try:
        n_dict_1 = cf.load_neighbours_dictionary_json(json_path=n_path_1)
    except IOError:
        return div_mat
    try:
        n_dict_2 = cf.load_neighbours_dictionary_json(json_path=n_path_2)
    except IOError:
        return div_mat
    try:
        m_dict = cf.load_matches(matches_path=m_path)
    except IOError:
        return div_mat

    n_dicts = {'dict_1': n_dict_1, 'dict_2': n_dict_2}

    divided_cells = []
    for cells in m_dict.itervalues():
        if len(cells) == 2:
            divided_cells.extend(cells)

    for mother_cell, daughter_cells in m_dict.iteritems():
        if len(daughter_cells) == 2:
            n_dicts_cp = copy.deepcopy(n_dicts)
            d_neigh_1 = n_dicts_cp['dict_2'][daughter_cells[0]]['neighbours']
            try:
                d_neigh_1.remove(daughter_cells[1])
            except ValueError:
                pass
            d_neigh_2 = n_dicts_cp['dict_2'][daughter_cells[1]]['neighbours']
            try:
                d_neigh_2.remove(daughter_cells[0])
            except ValueError:
                pass
            d_neigh_1.extend(d_neigh_2)
            if set(d_neigh_1).isdisjoint(set(divided_cells)):
                row = len(n_dicts['dict_1'][mother_cell]['neighbours'])
                col1 = len(n_dicts['dict_2'][daughter_cells[0]]['neighbours'])
                col2 = len(n_dicts['dict_2'][daughter_cells[1]]['neighbours'])
                div_mat[row, col1] += 1.0
                div_mat[row, col2] += 1.0

    return div_mat


def main():
    tps = [
        ['T00', 'T01'],
        ['T01', 'T02'],
        ['T02', 'T03'],
        ['T03', 'T04'],
        ['T04', 'T05'],
        ['T05', 'T06'],
        ['T06', 'T07'],
        ['T07', 'T08'],
        ['T08', 'T09'],
        ['T09', 'T10'],
        ['T10', 'T11'],
        ['T11', 'T12'],
        ['T12', 'T13'],
        ['T13', 'T14'],
        ['T14', 'T15'],
        ['T15', 'T16'],
        ['T16', 'T17'],
        ['T17', 'T18'],
        ['T18', 'T19'],
        ['T19', 'T20'],
        ['T20', 'T21'],
        ['T21', 'T22'],
        ['T22', 'T23'],
        ['T23', 'T24']
    ]
    #
    exp_ids = ['3002_PA', '3002_PD', '3078_PA', '3078_PC', '3148_PA', '3148_PD']

    t_matrix = np.zeros([12, 12], dtype=float)

    for exp_id in exp_ids:
        print exp_id
        for tp in tps:
            print tp
            temp_matrix = analyse_timepoint(exp_id, tp[0], tp[1])
            t_matrix += temp_matrix
    print t_matrix

    t_matrix_norm = np.zeros_like(t_matrix)

    for i, row in enumerate(t_matrix):
        row_sum = np.sum(row)
        if row_sum > 0:
            temp_row = row / row_sum
        else:
            temp_row = [0]*len(row)
        t_matrix_norm[i] = temp_row

    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    np.savetxt('../output/t_matrix.csv', t_matrix, delimiter=',')
    np.savetxt('../output/t_matrix_norm.csv', t_matrix_norm, delimiter=',')

if __name__ == '__main__':
    main()
