import argparse
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import os

import common_functions as cf


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    neighbours_dict = cf.load_neighbours_dictionary_json(json_path)
    cid_array = cf.path2id_array(segmented_image_path)

    del (neighbours_dict[0])

    outline_array = cf.generate_cell_outline_array(cid_array, color='black')

    upper_fraction = 0.45
    upper_pixels = upper_fraction * cid_array.shape[0]

    colour_array = np.full((cid_array.shape[0], cid_array.shape[1], 4),
                           [0, 0, 0, 1],
                           dtype=np.float)

    upper_cids, lower_cids = [], []
    all_cids = []

    edge_cells = []

    for cid, v in neighbours_dict.iteritems():
        if 0 not in v['neighbours']:
            all_cids.append(cid)
            if v['centroid_y'] < upper_pixels:
                upper_cids.append(cid)
            elif v['centroid_y'] > upper_pixels:
                lower_cids.append(cid)
            else:
                pass
        else:
            edge_cells.append(cid)

    upper_color = [float(255) / 255, float(255) / 255, float(102) / 255, 1]
    lower_color = [float(66) / 255, float(134) / 255, float(244) / 255, 1]

    for cid in neighbours_dict.iterkeys():
        if cid in upper_cids:
            colour_array[np.where(cid_array == cid)] = upper_color
        elif cid in lower_cids:
            colour_array[np.where(cid_array == cid)] = lower_color
        elif cid in edge_cells:
            colour_array[np.where(cid_array == cid)] = [.5, .5, .5, 1]
        else:
            pass

    all_hist = create_hist(all_cids, neighbours_dict)
    upper_hist = create_hist(upper_cids, neighbours_dict)
    lower_hist = create_hist(lower_cids, neighbours_dict)
    x = range(0, 18)

    fig = plt.figure(figsize=(7.5, 6))
    gs = gridspec.GridSpec(2, 2)
    ax1 = fig.add_subplot(gs[0:2, 0])
    ax1.imshow(colour_array)
    ax1.imshow(outline_array)
    cf.add_scale_bar(ax1)

    axlsize = 12
    ax2 = fig.add_subplot(gs[0, 1])
    ax2.plot(x, all_hist, color='gray', linewidth=10, alpha=0.5)
    ax2.plot(x, upper_hist, '-o', color='black', linewidth=2,
             markerfacecolor=upper_color, markersize=8)
    ax2.set_xticks(range(0, 10), minor=True)
    ax2.xaxis.set_ticklabels([])
    plt.tick_params(axis='both', which='major', labelsize=12)
    ax2.xaxis.grid(which='minor')
    ax2.set_ylabel("Frequency", fontsize=axlsize)
    plt.xlim([0, 10])

    ax3 = fig.add_subplot(gs[1, 1])
    ax3.plot(x, all_hist, color='gray', linewidth=10, alpha=0.5)
    ax3.plot(x, lower_hist, '-o', color='black', linewidth=2,
             markerfacecolor=lower_color, markersize=8)
    ax3.set_xticks(range(0, 10), minor=True)
    plt.tick_params(axis='both', which='major', labelsize=12)
    ax3.xaxis.grid(which='minor')
    ax3.set_xlabel("Number of Neighbours", fontsize=axlsize)
    ax3.set_ylabel("Frequency", fontsize=axlsize)
    plt.xlim([0, 10])

    plt.subplots_adjust(left=0.05, bottom=0.09, right=0.96, top=0.95)

    if args.outpath is not None:
        plt.savefig(os.path.join(args.outpath, "figure3b.png"), format='png', dpi=600)
    else:
        plt.show()


def create_hist(cids, dict):
    neighbours_list = []
    for cid in cids:
        neighbours_list.append(dict[cid]['num_neighbours'])

    neighbours_hist = [0] * 18
    for i in neighbours_list:
        neighbours_hist[i] += 1

    neighbours_hist[:] = [float(x) / sum(neighbours_hist) for x in neighbours_hist]

    return neighbours_hist


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00'")
    parser.add_argument("-o", "--outpath", help="name to save fig")
    args = parser.parse_args()

    main()
