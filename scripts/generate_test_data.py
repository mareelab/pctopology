import os

import numpy as np
from scipy.misc import imsave
import matplotlib.pyplot as plt


def main():

    path = "/Users/carterr/Dropbox/Postdoc_stuff/pcgeometry/data/raw/test_data"

    image = np.zeros(shape=[6, 6, 3])

    image[0:4, 0:2] = [0, 0, 1]
    image[0:2, 2:6] = [0, 0, 2]
    image[2:4, 2:4] = [0, 0, 3]
    image[2:6, 4:6] = [0, 0, 4]
    image[4:6, 0:4] = [0, 0, 5]

    image_2 = np.full([12, 12, 3], [0, 0, 0])
    image_2[2:8, 2:8] = image

    imsave(os.path.join(path, "T00.png"), image_2)

    plt.imshow(image_2, interpolation='nearest')
    plt.show()

if __name__ == "__main__":
    main()
