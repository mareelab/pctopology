#!usr/bin/env python
""" script for outputting a json file of cell neighbours, performs a
closing to deal with segmented images that have a cell border
"""

import os
import argparse
from collections import defaultdict
import json

import numpy as np
from skimage.morphology import square, closing
import skimage.measure as skim
# import matplotlib.pyplot as plt

import common_functions as ttf


def calc_centroid(cell_id, cid_array):

    cell_coords = np.where(cid_array == cell_id)

    centroid_x = np.mean(cell_coords[1])
    centroid_y = np.mean(cell_coords[0])

    return centroid_x, centroid_y


def output_neighbours():
    print "Generating Neighbourhood Dictionary"
    cid_array = ttf.path2id_array(args.segmented_image_path)
    ele_ord = 2

    cid_array = closing(cid_array, square(3))
    junction_array = np.zeros(cid_array.shape)
    neighbours_dictionary = defaultdict(list)

    for i in range(cid_array.shape[0] - ele_ord * 2):
        for j in range(cid_array.shape[1] - ele_ord * 2):
            for x, y in ttf.structure_element(i, j, ele_ord):
                if cid_array[i + ele_ord, j + ele_ord] is not cid_array[x, y]:
                    junction_array[x, y] = 2
                    neighbours_dictionary[cid_array[i + ele_ord, j + ele_ord]].append(cid_array[x, y])

    for cell_id, neighbours_list in neighbours_dictionary.iteritems():
        neighbours_dictionary[cell_id] = list(set(neighbours_list))
        neighbours_dictionary[cell_id].remove(cell_id)

    data_dict = {}

    for cell_id in neighbours_dictionary.iterkeys():
        data_dict[cell_id] = {}
        centroid_x, centroid_y = calc_centroid(cell_id, cid_array)
        data_dict[cell_id]['centroid_x'] = centroid_x
        data_dict[cell_id]['centroid_y'] = centroid_y
        data_dict[cell_id]['neighbours'] = neighbours_dictionary[cell_id]
        data_dict[cell_id]['area'] = sum(sum(cid_array == cell_id))

    cell_props = skim.regionprops(cid_array)
    for cell in cell_props:
        data_dict[cell['label']]['anisotropy'] = cell['major_axis_length']/cell['minor_axis_length']

    for cell_id, data_dict2 in data_dict.iteritems():
        area_list = []
        nl = list(data_dict2['neighbours'])
        while 0 in nl:
            nl.remove(0)
        for neighbour_id in nl:
            area_list.append(data_dict[neighbour_id]['area'])
        data_dict[cell_id]['num_neighbours'] = len(nl)
        norm_area = sum(area_list) / (len(area_list) + 0.00000000001)
        data_dict[cell_id]['norm_area'] = data_dict2['area'] / float(norm_area) + 0.000000001

    for cell_id, data_dict2 in data_dict.iteritems():
        neighbour_norm_list = []
        nl = list(data_dict2['neighbours'])
        while 0 in nl:
            nl.remove(0)
        for neighbour_id in nl:
            neighbour_norm_list.append(data_dict[neighbour_id]['norm_area'])
        av_neighbour_normed_area = sum(neighbour_norm_list) / (len(neighbour_norm_list) + 0.000000001)
        data_dict[cell_id]['av_neighbour_normed_area'] = av_neighbour_normed_area

    data_dict_for_output = {int(k): v for k, v in data_dict.iteritems()}

    out_filename = os.path.basename(args.segmented_image_path).split(".")[-2] + "_neighbours.json"
    output_path = os.path.join(os.path.dirname(args.segmented_image_path), out_filename)

    with open(output_path, 'w') as output_handle:
        json.dump(data_dict_for_output, output_handle, cls=ttf.numpy_JSON_Encoder, sort_keys=True, indent=4)

    return 0

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("segmented_image_path", help="Segmented image")
    # parser.add_argument("output_dir", help="Output directory")
    args = parser.parse_args()

    output_neighbours()
