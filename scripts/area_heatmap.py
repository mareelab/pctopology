#!/usr/local/bin/python
"""plots a heatmap of normalised area and number of neighbours for a leaf"""
import argparse
import os
import ast

import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.colors
import matplotlib.cm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

import common_functions as ttf
from fig_conserved_topo import create_hist


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    md_dict = False
    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        metadata_path = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (args.time_point + '.txt'))
        md_dict = ttf.load_metadata(metadata_path)

    cid_array = ttf.path2id_array(segmented_image_path)
    neighbourhood_dictionary = ttf.load_neighbours_dictionary_json(json_path)
    del neighbourhood_dictionary[0]

    area_list = []
    norm_area_list = []
    num_neighbours_list = []
    cids = []

    for cid, v1 in neighbourhood_dictionary.iteritems():
        norm_area_list.append(v1['norm_area'])
        if md_dict:
            area_list.append(v1['area'] * md_dict['vox_x'] * md_dict['vox_y'])
        else:
            area_list.append(v1['area'])
        if 0 not in v1['neighbours']:
            num_neighbours_list.append(v1['num_neighbours'])
            cids.append(cid)

    for index, area in enumerate(norm_area_list):
        if area == np.inf:
            norm_area_list[index] = 1

    heatmap_shape = [cid_array.shape[0], cid_array.shape[1], 4]

    area_heatmap = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float32')
    norm_area_heatmap = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float32')
    neigh_heatmap = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float32')

    area_min = min(area_list)
    area_max = max(area_list)

    norm_area_min = min(norm_area_list)
    norm_area_max = max(norm_area_list)

    color_scheme_1 = 'viridis'
    if args.limits is None:
        n_min, n_max = get_clim(num_neighbours_list)
    else:
        n_lim = ast.literal_eval(args.limits)
        n_min, n_max = n_lim[0], n_lim[1]

    levels = n_max - n_min + 1
    color_scheme_2 = matplotlib.cm.get_cmap(name='BrBG', lut=levels)

    color_map_area = matplotlib.cm.ScalarMappable(cmap=color_scheme_1)
    color_map_area.set_clim(vmin=area_min, vmax=area_max)

    color_map_norm_area = matplotlib.cm.ScalarMappable(cmap=color_scheme_1)
    color_map_norm_area.set_clim(vmin=norm_area_min, vmax=norm_area_max)

    color_map_neighbour = matplotlib.cm.ScalarMappable(cmap=color_scheme_2)
    color_map_neighbour.set_clim(vmin=n_min, vmax=n_max)

    for cid, data in neighbourhood_dictionary.iteritems():
        if md_dict:
            area_heatmap[cid_array == cid] = color_map_area.to_rgba(data['area'] * md_dict['vox_x'] * md_dict['vox_y'])
        else:
            area_heatmap[cid_array == cid] = color_map_area.to_rgba(data['area'])

        norm_area_heatmap[cid_array == cid] = color_map_norm_area.to_rgba(data['norm_area'])

        if 0 not in neighbourhood_dictionary[cid]['neighbours']:
            neigh_heatmap[cid_array == cid] = color_map_neighbour.to_rgba(data['num_neighbours'])
        else:
            neigh_heatmap[cid_array == cid] = [128, 128, 128, 1]

    cell_outlines = ttf.generate_cell_outline_array(cid_array)

    plt.figure(facecolor='white', figsize=(20, 10))

    path = segmented_image_path.split('/')
    title_string = path[-2] + " " + path[-1][:-4]

    if os.path.exists(os.path.join(data_dir, args.exp_id, 'microscope_metadata')):
        extent = [0, norm_area_heatmap.shape[1] * md_dict['vox_y'], 0, norm_area_heatmap.shape[0] * md_dict['vox_x']]
    else:
        extent = [0, norm_area_heatmap.shape[1], 0, norm_area_heatmap.shape[0]]

    font_size = 18
    cbarlsize = 20

    plt.figure(figsize=(20, 10), facecolor='white')
    ax1 = plt.subplot(1, 3, 1)
    plt.title(r'Area, $\mu$m$^2$', fontsize=font_size)
    plt.imshow(area_heatmap, cmap=color_scheme_1, interpolation='nearest', extent=extent)
    plt.imshow(cell_outlines, cmap=color_scheme_1, interpolation='nearest', extent=extent)
    plt.clim(area_min, area_max)
    plt.xlabel(r'x, $\mu$m', fontsize=font_size)
    plt.ylabel(r'y, $\mu$m', fontsize=font_size)
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="5%", pad=0.05)
    cbar1 = plt.colorbar(cax=cax1)
    cbar1.ax.tick_params(labelsize=cbarlsize)
    ttf.add_scale_bar(ax1)

    ax2 = plt.subplot(1, 3, 2)
    plt.title('Normalised Area', fontsize=font_size)
    plt.imshow(norm_area_heatmap, cmap=color_scheme_1, interpolation='nearest', extent=extent)
    plt.imshow(cell_outlines, cmap=color_scheme_1, interpolation='nearest', extent=extent)
    plt.clim(norm_area_min, norm_area_max)
    plt.xlabel(r'x, $\mu$m', fontsize=font_size)
    plt.ylabel(r'y, $\mu$m', fontsize=font_size)
    divider2 = make_axes_locatable(ax2)
    cax2 = divider2.append_axes("right", size="5%", pad=0.05)
    cbar2 = plt.colorbar(cax=cax2)
    cbar2.ax.tick_params(labelsize=cbarlsize)
    ttf.add_scale_bar(ax2)

    ax3 = plt.subplot(1, 3, 3)
    plt.title('Number of Neighbours', fontsize=font_size)
    im3 = plt.imshow(neigh_heatmap, cmap=color_scheme_2, interpolation='nearest', extent=extent,
                     vmin=n_min - .5, vmax=n_max + .5)
    plt.imshow(cell_outlines, cmap=color_scheme_2, interpolation='nearest', extent=extent)
    plt.clim(n_min - .5, n_max + .5)
    plt.xlabel(r'x, $\mu$m', fontsize=font_size)
    plt.ylabel(r'y, $\mu$m', fontsize=font_size)
    divider3 = make_axes_locatable(ax3)
    cax3 = divider3.append_axes("right", size="5%", pad=0.05)
    cbar3 = plt.colorbar(im3, cax=cax3, ticks=np.arange(n_min, n_max + 1))
    cbar3.ax.tick_params(labelsize=cbarlsize)
    ttf.add_scale_bar(ax3)

    plt.subplots_adjust(left=0.04, bottom=0.01, right=0.96, top=0.96, wspace=0.2, hspace=0.2)

    if args.outname is None:
        plt.figure(facecolor='white', figsize=(20, 10))
        ax4 = plt.subplot(1, 2, 1)
        plt.title('Number of neighbours')
        im4 = plt.imshow(neigh_heatmap, cmap=color_scheme_2, interpolation='nearest', extent=extent,
                         vmin=n_min - .5, vmax=n_max + .5)
        plt.imshow(cell_outlines, cmap=color_scheme_2, interpolation='nearest', extent=extent)
        plt.clim(n_min - .5, n_max + .5)
        plt.xlabel(r'x, $\mu$m')
        plt.ylabel(r'y, $\mu$m')
        divider4 = make_axes_locatable(ax4)
        cax4 = divider4.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(im4, cax=cax4, ticks=np.arange(n_min, n_max + 1))

        ax5 = plt.subplot(1, 2, 2)
        hist = create_hist(cids, neighbourhood_dictionary)
        x = range(0, 18)
        ax5.plot(x, hist, color='gray', linewidth=8, alpha=0.5)
        plt.xlim([0, 10])
        plt.xlabel("Number of Neighbours")
        plt.ylabel("Frequency")
        plt.subplots_adjust(left=0.00, bottom=0.06, right=0.96, top=0.95, wspace=0.04, hspace=0.2)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname), format='png', dpi=600)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00', 'T01' etc")
    parser.add_argument("-n", "--limits", help="colorbar_limits i.e. [0,12]")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    main()
