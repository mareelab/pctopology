#!/usr/bin/python
"""plots a heatmap of normalised area and number of neighbours for a leaf"""
import argparse
import os
import ast

import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.colors
import matplotlib.cm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

import common_functions as ttf


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def main(args):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    metadata_path = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (args.time_point + '.txt'))
    md_dict = ttf.load_metadata(metadata_path)

    cid_array = ttf.path2id_array(segmented_image_path)
    neighbourhood_dictionary = ttf.load_neighbours_dictionary_json(json_path)
    if 0 in neighbourhood_dictionary.keys():
        del neighbourhood_dictionary[0]

    num_neighbours_list = []
    cids = []

    for cid, v1 in neighbourhood_dictionary.iteritems():
        if 0 not in v1['neighbours']:
            num_neighbours_list.append(v1['num_neighbours'])
            cids.append(cid)

    heatmap_shape = [cid_array.shape[0], cid_array.shape[1], 4]

    neigh_heatmap = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float32')

    if args.limits is None:
        n_min, n_max = get_clim(num_neighbours_list)
    else:
        n_lim = ast.literal_eval(args.limits)
        n_min, n_max = n_lim[0], n_lim[1]

    levels = n_max - n_min + 1
    color_scheme_2 = matplotlib.cm.get_cmap(name='BrBG', lut=levels)

    color_map_neighbour = matplotlib.cm.ScalarMappable(cmap=color_scheme_2)
    color_map_neighbour.set_clim(vmin=n_min, vmax=n_max)

    for cid, data in neighbourhood_dictionary.iteritems():
        if 0 not in neighbourhood_dictionary[cid]['neighbours']:
            neigh_heatmap[cid_array == cid] = color_map_neighbour.to_rgba(data['num_neighbours'])
        else:
            neigh_heatmap[cid_array == cid] = [128, 128, 128, 1]

    cell_outlines = ttf.generate_cell_outline_array(cid_array)

    extent = [0, neigh_heatmap.shape[1] * md_dict['vox_y'], 0, neigh_heatmap.shape[0] * md_dict['vox_x']]

    plt.figure(facecolor='white', figsize=(8, 13))
    ax4 = plt.subplot(1, 1, 1)
    im4 = plt.imshow(neigh_heatmap, cmap=color_scheme_2, interpolation='nearest', extent=extent,
                     vmin=n_min - .5, vmax=n_max + .5)
    plt.imshow(cell_outlines, cmap=color_scheme_2, interpolation='nearest', extent=extent)
    plt.clim(n_min - .5, n_max + .5)
    size = ttf.add_scale_bar(ax4)
    divider4 = make_axes_locatable(ax4)
    cax4 = divider4.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im4, cax=cax4, ticks=np.arange(n_min, n_max + 1))
    if 'S9' in args.outname:
        cbar.ax.tick_params(labelsize=15)
        plt.subplots_adjust(left=0.10, bottom=0.05, right=0.93, top=0.92, wspace=0.2, hspace=0.2)
    else:
        cbar.ax.tick_params(labelsize=30)
        plt.subplots_adjust(left=0.10, bottom=0.05, right=0.96, top=0.92, wspace=0.2, hspace=0.2)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname), dpi=600)




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00', 'T01' etc")
    parser.add_argument("-n", "--limits", help="colorbar_limits i.e. [0,12]")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    main(args)
