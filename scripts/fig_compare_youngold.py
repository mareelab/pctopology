#!/usr/bin/python
"""plots a heatmap of normalised area and number of neighbours for a leaf"""
import argparse
import os
import ast

import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.colors
import matplotlib.cm
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import ticker
import numpy as np

import common_functions as ttf
from fig_conserved_topo import create_hist


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def generate_neigh_heatmap(timepoint, lims=None):
    data_dir = '../data/raw'
    json_path_1 = os.path.join(data_dir, args.exp_id, (timepoint + '_neighbours.json'))
    segmented_image_path_1 = os.path.join(data_dir, args.exp_id, (timepoint + '.png'))

    metadata_path_1 = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (timepoint + '.txt'))
    md_dict_1 = ttf.load_metadata(metadata_path_1)

    cid_array = ttf.path2id_array(segmented_image_path_1)
    neighbourhood_dictionary = ttf.load_neighbours_dictionary_json(json_path_1)
    del neighbourhood_dictionary[0]

    num_neighbours_list = []
    cids = []

    for cid, v1 in neighbourhood_dictionary.iteritems():
        if 0 not in v1['neighbours']:
            num_neighbours_list.append(v1['num_neighbours'])
            cids.append(cid)

    heatmap_shape = [cid_array.shape[0], cid_array.shape[1], 4]
    neigh_heatmap = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float')

    if lims is None:
        n_min, n_max = get_clim(num_neighbours_list)
    else:
        n_min, n_max = lims[0], lims[1]

    levels = n_max - n_min + 1
    colour_scheme = matplotlib.cm.get_cmap(name='BrBG', lut=levels)
    color_map_neighbour = matplotlib.cm.ScalarMappable(cmap=colour_scheme)
    color_map_neighbour.set_clim(vmin=n_min, vmax=n_max)

    for cid, data in neighbourhood_dictionary.iteritems():
        if 0 not in neighbourhood_dictionary[cid]['neighbours']:
            neigh_heatmap[cid_array == cid] = color_map_neighbour.to_rgba(data['num_neighbours'])
        else:
            neigh_heatmap[cid_array == cid] = [128, 128, 128, 1]

    cell_outlines = ttf.generate_cell_outline_array(cid_array)

    n_list = [n_min, n_max]

    extent = [0, neigh_heatmap.shape[1] * md_dict_1['vox_y'], 0, neigh_heatmap.shape[0] * md_dict_1['vox_x']]
    return neigh_heatmap, cell_outlines, extent, colour_scheme, n_list, cids, neighbourhood_dictionary


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    neigh_heatmap_1, cell_outlines_1, extent_1, colour_scheme_1, n_list_1, \
    cids_1, neigh_dict_1 = generate_neigh_heatmap(args.time_point_1, [0, 12])

    neigh_heatmap_2, cell_outlines_2, extent_2, colour_scheme_2, n_list_2, \
    cids_2, neigh_dict_2 = generate_neigh_heatmap(args.time_point_2)

    plt.figure(facecolor='white', figsize=(20, 10))

    cbarlsize = 20
    ax4 = plt.subplot(1, 3, 1)
    im4 = plt.imshow(neigh_heatmap_1, cmap=colour_scheme_1, interpolation='nearest', extent=extent_1,
                     vmin=n_list_1[0] - .5, vmax=n_list_1[1] + .5)
    plt.imshow(cell_outlines_1, cmap=colour_scheme_1, interpolation='nearest', extent=extent_1)
    plt.clim(n_list_1[0] - .5, n_list_1[1] + .5)
    divider4 = make_axes_locatable(ax4)
    cax4 = divider4.append_axes("right", size="5%", pad=0.05)
    cbar1 = plt.colorbar(im4, cax=cax4, ticks=np.arange(n_list_1[0], n_list_1[1] + 1))
    cbar1.ax.tick_params(labelsize=cbarlsize)
    ttf.add_scale_bar(ax4)

    ax1 = plt.subplot(1, 3, 2)
    im1 = plt.imshow(neigh_heatmap_2, cmap=colour_scheme_2, interpolation='nearest', extent=extent_2,
                     vmin=n_list_2[0] - .5, vmax=n_list_2[1] + .5)
    plt.imshow(cell_outlines_2, cmap=colour_scheme_2, interpolation='nearest', extent=extent_2)
    plt.clim(n_list_2[0] - .5, n_list_2[1] + .5)
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="5%", pad=0.05)
    cbar2 = plt.colorbar(im1, cax=cax1, ticks=np.arange(n_list_2[0], n_list_2[1] + 1))
    cbar2.ax.tick_params(labelsize=cbarlsize)
    ttf.add_scale_bar(ax1)

    axlsize = 18
    ax5 = plt.subplot(1, 3, 3)
    hist_1 = create_hist(cids_1, neigh_dict_1)
    hist_2 = create_hist(cids_2, neigh_dict_2)
    x = range(0, 18)
    ax5.plot(x, hist_1, color='blue', linewidth=8, alpha=0.5, label='Young')
    ax5.plot(x, hist_2, color='red', linewidth=8, alpha=0.5, label='Old')
    plt.xlim([1, 10])
    plt.legend(prop={'size': 18})
    plt.xlabel("Number of Neighbours", fontsize=axlsize)
    plt.ylabel("Frequency", fontsize=axlsize)
    ax5.set_xticks(range(1, 10), minor=True)
    ax5.xaxis.grid(which='minor')
    plt.tick_params(axis='both', which='major', labelsize=20)
    plt.subplots_adjust(left=0.05, bottom=0.08, right=0.96, top=0.95, wspace=0.3)

    if args.outpath is not None:
        plt.savefig(os.path.join(args.outpath, "figure3a.png"), format='png', dpi=600)
    else:
        plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point_1", help="Time point in the experiment. eg. 'T00', 'T01' etc")
    parser.add_argument("time_point_2", help="second time point in the experiment. eg. 'T00', 'T01' etc")
    parser.add_argument("-o", "--outpath", help="name to save fig")

    args = parser.parse_args()

    main()
