import os
import copy
import argparse

import matplotlib.colors
import matplotlib.cm
import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np

import common_functions as cf


def crop_images(image, rect):
    return image[rect[0]:rect[1], rect[2]:rect[3]]


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def show_divisions(im_dict, n_dicts, md_dict, mother_cell, daughter_cells, path=None):
    points1 = np.where(im_dict['id_arr_1'] == mother_cell)
    points2a = np.where(im_dict['id_arr_2'] == daughter_cells[0])
    points2b = np.where(im_dict['id_arr_2'] == daughter_cells[1])

    border = 6
    y1_min = min(points1[0]) - border
    y1_max = max(points1[0]) + border
    x1_min = min(points1[1]) - border
    x1_max = max(points1[1]) + border
    y2_max = max(max(points2a[0]), max(points2b[0])) + border
    y2_min = min(min(points2a[0]), min(points2b[0])) - border
    x2_max = max(max(points2a[1]), max(points2b[1])) + border
    x2_min = min(min(points2a[1]), min(points2b[1])) - border

    id_array_1 = crop_images(im_dict['id_arr_1'], [y1_min, y1_max, x1_min, x1_max])
    ol_1 = crop_images(im_dict['ol_arr_1'], [y1_min, y1_max, x1_min, x1_max])
    id_array_2 = crop_images(im_dict['id_arr_2'], [y2_min, y2_max, x2_min, x2_max])
    ol_2 = crop_images(im_dict['ol_arr_2'], [y2_min, y2_max, x2_min, x2_max])
    im_1 = crop_images(im_dict['im_arr_1'], [y1_min, y1_max, x1_min, x1_max])
    im_2 = crop_images(im_dict['im_arr_2'], [y2_min, y2_max, x2_min, x2_max])

    neighs_1 = np.unique(id_array_1)
    neighs_2 = np.unique(id_array_2)

    n_list_1, n_list_2 = list(), list()

    for n in neighs_1:
        n_list_1.append(len(n_dicts['dict_1'][n]['neighbours']))

    for n in neighs_2:
        n_list_2.append(len(n_dicts['dict_2'][n]['neighbours']))

    clim_1 = get_clim(n_list_1)
    clim_2 = get_clim(n_list_2)

    clim_min = min(clim_1[0], clim_2[0])
    clim_max = max(clim_1[1], clim_2[1])
    clim = [clim_min, clim_max]

    levels_1 = clim[1] - clim[0] + 1
    col_scheme_1 = matplotlib.cm.get_cmap(name='BrBG', lut=levels_1)

    col_map_1 = matplotlib.cm.ScalarMappable(cmap=col_scheme_1)
    col_map_1.set_clim(vmin=clim[0], vmax=clim[1])

    levels_2 = clim[1] - clim[0] + 1
    col_scheme_2 = matplotlib.cm.get_cmap(name='BrBG', lut=levels_2)

    col_map_2 = matplotlib.cm.ScalarMappable(cmap=col_scheme_2)
    col_map_2.set_clim(vmin=clim[0], vmax=clim[1])

    neigh_arr_1 = np.zeros(shape=[id_array_1.shape[0], id_array_1.shape[1], 4])
    neigh_arr_2 = np.zeros(shape=[id_array_2.shape[0], id_array_2.shape[1], 4])

    for n in neighs_1:
        if n is not 0:
            n_neighbours = len(n_dicts['dict_1'][n]['neighbours'])
            neigh_arr_1[id_array_1 == n] = col_map_1.to_rgba(n_neighbours)

    for n in neighs_2:
        if n is not 0:
            n_neighbours = len(n_dicts['dict_2'][n]['neighbours'])
            neigh_arr_2[id_array_2 == n] = col_map_2.to_rgba(n_neighbours)

    extent1 = [0, neigh_arr_1.shape[1] * md_dict['md1']['vox_y'], 0, neigh_arr_1.shape[0] * md_dict['md1']['vox_x']]
    extent2 = [0, neigh_arr_2.shape[1] * md_dict['md2']['vox_y'], 0, neigh_arr_2.shape[0] * md_dict['md2']['vox_x']]

    plt.figure(facecolor='white', figsize=(20, 4))

    lsize = 20
    ax1 = plt.subplot(142)
    plt.imshow(neigh_arr_1, interpolation='nearest', cmap=col_scheme_1, extent=extent1)
    plt.imshow(ol_1, interpolation='nearest', cmap=col_scheme_1, extent=extent1)
    plt.clim(clim[0]-0.5, clim[1]+0.5)
    cbar1 = plt.colorbar(ticks=np.arange(clim[0], clim[1] + 1))
    cbar1.ax.tick_params(labelsize=lsize)
    ax1.axis('off')

    ax2 = plt.subplot(144)
    plt.imshow(neigh_arr_2, interpolation='nearest', cmap=col_scheme_2, extent=extent2)
    plt.imshow(ol_2, interpolation='nearest', cmap=col_scheme_2, extent=extent2)
    plt.clim(clim[0]-0.5, clim[1]+0.5)
    cbar2 = plt.colorbar(ticks=np.arange(clim[0], clim[1] + 1))
    cbar2.ax.tick_params(labelsize=lsize)
    ax2.axis('off')

    ax3 = plt.subplot(141)
    ax3.imshow(im_1, interpolation='nearest', cmap='gray', extent=extent1)
    cf.add_scale_bar(ax3)

    ax4 = plt.subplot(143)
    ax4.imshow(im_2, interpolation='nearest', cmap='gray', extent=extent2)
    cf.add_scale_bar(ax4)

    plt.subplots_adjust(left=0.04, bottom=0.05, right=0.96, top=0.95, wspace=0.2, hspace=0.2)

    if path is not None:
        plt.savefig(path, format='png', dpi=600)
    else:
        plt.show()


def analyse_timepoint(exp_id, tp1, tp2, visualise=True):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    n_path_1 = os.path.join(data_dir, exp_id, (tp1 + '_neighbours.json'))
    n_path_2 = os.path.join(data_dir, exp_id, (tp2 + '_neighbours.json'))

    m_path = os.path.join(data_dir, exp_id, 'cell_matches', (tp1 + tp2 + '.match'))

    n_dict_1 = cf.load_neighbours_dictionary_json(json_path=n_path_1)
    n_dict_2 = cf.load_neighbours_dictionary_json(json_path=n_path_2)
    m_dict = cf.load_matches(matches_path=m_path)

    n_dicts = {'dict_1': n_dict_1, 'dict_2': n_dict_2}
    im_dict = {}
    metadata_dict = {}
    if visualise:
        seg_im_1_path = os.path.join(data_dir, exp_id, (tp1 + '.png'))
        seg_im_2_path = os.path.join(data_dir, exp_id, (tp2 + '.png'))
        id_arr_1 = cf.path2id_array(image_path=seg_im_1_path)
        id_arr_2 = cf.path2id_array(image_path=seg_im_2_path)

        im_arr_path_1 = os.path.join(data_dir, exp_id, 'images', tp1 + '.png')
        im_arr_path_2 = os.path.join(data_dir, exp_id, 'images', tp2 + '.png')
        im_arr_1 = cf.path2image_array(image_path=im_arr_path_1)
        im_arr_2 = cf.path2image_array(image_path=im_arr_path_2)

        ol_arr_1 = cf.generate_cell_outline_array(cid_array=id_arr_1)
        ol_arr_2 = cf.generate_cell_outline_array(cid_array=id_arr_2)

        md_path_1 = os.path.join(data_dir, exp_id, 'microscope_metadata', (tp1 + '.txt'))
        md_path_2 = os.path.join(data_dir, exp_id, 'microscope_metadata', (tp2 + '.txt'))
        metadata_dict_1 = cf.load_metadata(md_path_1)
        metadata_dict_2 = cf.load_metadata(md_path_2)
        metadata_dict = {'md1': metadata_dict_1, 'md2': metadata_dict_2}

        im_dict = {
            'id_arr_1': id_arr_1,
            'id_arr_2': id_arr_2,
            'ol_arr_1': ol_arr_1,
            'ol_arr_2': ol_arr_2,
            'im_arr_1': im_arr_1,
            'im_arr_2': im_arr_2
        }

    divided_cells = []
    for cells in m_dict.itervalues():
        if len(cells) == 2:
            divided_cells.extend(cells)

    div_mat = np.zeros([20, 20])

    if args.specific is None:
        for mother_cell, daughter_cells in m_dict.iteritems():
            if len(daughter_cells) == 2:
                n_dicts_cp = copy.deepcopy(n_dicts)
                d_neigh_1 = n_dicts_cp['dict_2'][daughter_cells[0]]['neighbours']
                d_neigh_1.remove(daughter_cells[1])
                d_neigh_2 = n_dicts_cp['dict_2'][daughter_cells[1]]['neighbours']
                d_neigh_2.remove(daughter_cells[0])

                d_neigh_1.extend(d_neigh_2)
                if set(d_neigh_1).isdisjoint(set(divided_cells)):
                    row = len(n_dicts['dict_1'][mother_cell]['neighbours'])
                    col1 = len(n_dicts['dict_2'][daughter_cells[0]]['neighbours'])
                    col2 = len(n_dicts['dict_2'][daughter_cells[1]]['neighbours'])
                    div_mat[row, col1] += 1
                    div_mat[row, col2] += 1
                    # print 'Mother neighbours: ', row
                    # print 'Daughter Neighbours: ', col1, col2

                    if visualise:
                        show_divisions(im_dict, n_dicts, metadata_dict, mother_cell, daughter_cells)

    if args.specific is not None:
        specific_m_cell = 2138
        specific_d_cells = [2117, 2075]
        if args.outname is not None:
            path = os.path.join(output_dir, args.outname)
        else:
            path = None
        show_divisions(im_dict, n_dicts, metadata_dict, specific_m_cell, specific_d_cells, path)
    # print div_mat[0:10, 0:10]

    return div_mat


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("tp1", help="Time point in the experiment. eg. 'T00'")
    parser.add_argument("tp2", help="Time point in the experiment. eg. 'T01'")
    parser.add_argument("-s", "--specific", help="show the division for the paper figure")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    analyse_timepoint(args.exp_id, args.tp1, args.tp2, visualise=True)
