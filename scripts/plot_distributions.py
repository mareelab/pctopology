import argparse
import os
import json

import matplotlib.pyplot as plt


def import_model_results(path):
    with open(path, 'r') as import_handle:
        dictionary = json.load(import_handle)
    data_list = [0] * len(dictionary)
    for k, v in dictionary.iteritems():
        data_list[int(k)] = v
    return data_list


def import_extracted_data(path):
    with open(path, 'r') as import_handle:
        dictionary = json.load(import_handle)
    return dictionary


def import_to_distribution(path):
    with open(path, 'r') as import_handle:
        dictionary = json.load(import_handle)

    dist_list = [0]*40

    del dictionary['0']
    for k, v in dictionary.iteritems():
        if 0 not in v["neighbours"]:
            nn = int(v['num_neighbours'])
            dist_list[nn] += 1.0

    tot = sum(dist_list)
    norm_list = [i/tot for i in dist_list]

    return norm_list


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    pascal_sync = import_model_results("../output/model_results/pascal_sync.json")
    random_sync = import_model_results("../output/model_results/random_sync.json")
    equal_sync = import_model_results("../output/model_results/equal_div_sync.json")
    equal_async = import_model_results("../output/model_results/equal_div_async.json")

    arabidopsis_data = import_model_results("../output/arabidopsis_distributions.json")

    extracted = import_extracted_data("../output/extracted_data.json")

    jonsson_extracted = import_extracted_data("../output/jonsson_extracted.json")

    x = range(len(pascal_sync))
    x_limits = [1, 12]
    y_limits = [0, 0.4]
    
    arabidopsis_color = 'green'
    equal_sync_color = 'blue'
    pascal_sync_color = 'red'
    random_sync_color = 'purple'
    equal_async_color = 'orange'
    m_size = 4
    leg_fontsize = 9
    axlsize=14
    xyticksize=12
    
    if args.distribution == 'model_comp':
        fig1, ax1 = plt.subplots(figsize=(5, 4))
        ax1.plot(x, equal_sync, '-o', color=equal_sync_color, linewidth=2,
                 markerfacecolor=equal_sync_color, markersize=m_size, label='Equal')
        ax1.plot(x, pascal_sync, '-o', color=pascal_sync_color, linewidth=2,
                 markerfacecolor=pascal_sync_color, markersize=m_size, label='Pascal')
        ax1.plot(x, random_sync, '-o', color=random_sync_color, linewidth=2,
                 markerfacecolor=random_sync_color, markersize=m_size, label='Random')
        ax1.plot(range(len(arabidopsis_data)), arabidopsis_data, '-o', color=arabidopsis_color, linewidth=2,
                 markerfacecolor=arabidopsis_color, markersize=m_size, label='$\it{spch}$ Arabidopsis Leaf')
        ax1.set_xlim(x_limits)
        ax1.set_ylim(y_limits)
        ax1.set_xticks(range(x_limits[0], x_limits[1]), minor=True)
        ax1.xaxis.grid(which='minor')
        ax1.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax1.set_ylabel("Frequency", fontsize=axlsize)
        ax1.legend(loc=1, fontsize=leg_fontsize)
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)

        fig1.tight_layout()

        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figure5a.png"), format='png', dpi=600)

    if args.distribution == 'data_comp':
        fig2, ax2 = plt.subplots()
        ax2.plot(x, equal_sync, '-o', color='blue', linewidth=2,
                 markerfacecolor='blue', markersize=8, label='Equal')
        ax2.plot(range(len(arabidopsis_data)), arabidopsis_data, '-o', color='green', linewidth=2,
                 markerfacecolor='green', markersize=8, label='Arabidopsis')
        # arabadopsis data
        ax2.set_xlim(x_limits)
        ax2.set_ylim(y_limits)
        ax2.set_xticks(range(x_limits[0], x_limits[1]), minor=True)
        ax2.xaxis.grid(which='minor')
        ax2.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax2.set_ylabel("Frequency", fontsize=axlsize)
        ax2.legend(loc=1, fontsize=leg_fontsize)
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)
        fig2.tight_layout()

        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figure5b.png"), format='png', dpi=600)

    if args.distribution == 'extracted_data':
        fig3, ax3 = plt.subplots(figsize=(6, 7))
        x_limits_2 = [2, 11]
        y_limits_2 = [0, 0.7]

        x2 = range(len(extracted['mombach_anthurium']))
        x3 = range(len(extracted['gibson_cucumber']))
        x4 = range(len(arabidopsis_data))
        ax3.plot(x4, arabidopsis_data, label='$\it{spch}$ Arabidopsis Leaf', linewidth=4)
        ax3.plot(x2, extracted['mombach_anthurium'], label='Anthurium', linewidth=2)
        ax3.plot(x2, extracted['mombach_cepa'], label='Cepa', linewidth=2)
        ax3.plot(x2, extracted['mombach_attenuata'], label='Attenuata', linewidth=2)
        ax3.plot(x2, extracted['mombach_sativum'], label='Sativum', linewidth=2)
        # ax3.plot(x2, extracted['mombach_aborescens'], label='Aborescens', linewidth=2)
        # ax3.plot(x3, extracted['gibson_cucumber'], '-.', label='Cucumber', linewidth=2)
        ax3.plot(x3, extracted['gibson_drosophila'], '-.', label='Drosophila', linewidth=2)
        ax3.plot(x3, extracted['gibson_xenopus'], '-.', label='Xenopus', linewidth=2)
        ax3.set_xlim(x_limits_2)
        ax3.set_ylim(y_limits_2)
        ax3.set_xticks(range(x_limits_2[0], x_limits_2[1]), minor=True)
        ax3.xaxis.grid(which='minor')
        ax3.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax3.set_ylabel("Frequency", fontsize=axlsize)
        ax3.legend(loc=1, fontsize=11)
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)
        fig3.tight_layout()

        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figure3c.png"), format='png', dpi=600)

    if args.distribution == 'picking_behaviour':
        fig4, ax4 = plt.subplots(figsize=(5, 4))
        
        ax4.plot(x, equal_sync, '-o', color=equal_sync_color, linewidth=2,
                 markerfacecolor=equal_sync_color, markersize=m_size, label='w/o replacement')
        ax4.plot(range(len(arabidopsis_data)), arabidopsis_data, '-o', color=arabidopsis_color, linewidth=2,
                 markerfacecolor=arabidopsis_color, markersize=m_size, label='$\it{spch}$ Arabidopsis Leaf')
        ax4.plot(x, equal_async, '-o', color=equal_async_color, linewidth=2,
                 markerfacecolor=equal_async_color, markersize=m_size, label='with replacement')

        ax4.set_xlim(x_limits)
        ax4.set_ylim(y_limits)
        ax4.set_xticks(range(x_limits[0], x_limits[1]), minor=True)
        ax4.xaxis.grid(which='minor')
        ax4.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax4.set_ylabel("Frequency", fontsize=axlsize)
        ax4.legend(loc=1, fontsize=leg_fontsize)
        fig4.tight_layout()
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)
        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figure5b.png"), format='png', dpi=600)

    if args.distribution == 'comp_jonsson_all':
        # TODO check colors of distributions
        fig5, ax5 = plt.subplots(figsize=(7.5, 6))
        x = range(3, 12)
        for name, dist in jonsson_extracted.iteritems():
            if 'spch' in name:
                ax5.plot(x, dist, '-o', linewidth=3, markersize=m_size, label='$\it{spch}$ Arabidopsis Leaf')
            elif 'Topology' in name:
                ax5.plot(x, dist, '-o', linewidth=3, markersize=m_size, label=name)
            else:
                pass
                
        for name, dist in jonsson_extracted.iteritems():
            if 'spch' in name:
                pass
            elif 'Topology' in name:
                pass
            else:
                ax5.plot(x, dist, '-o', linewidth=2, markersize=m_size, label=name, alpha=0.5)

        ax5.set_xlim([3, 11])
        ax5.set_ylim([0, 0.55])
        ax5.set_xticks(range(3, 11), minor=True)
        ax5.xaxis.grid(which='minor')
        ax5.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax5.set_ylabel("Frequency", fontsize=axlsize)
        ax5.legend(loc=1, fontsize=11)
        fig5.tight_layout()
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)
        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figureS1a.png"), format='png', dpi=600)

    if args.distribution == 'comp_jonsson':
        fig5, ax5 = plt.subplots(figsize=(7.5, 6))
        x = range(3, 12)
        ax5.plot(x, jonsson_extracted['spch Arabidopsis Leaf'], '-o', linewidth=2,
                 markersize=m_size, label='$\it{spch}$ Arabidopsis Leaf')
        ax5.plot(x, jonsson_extracted['Topology Model'], '-o', linewidth=2,
                 markersize=m_size, label='Topology Model')
        ax5.plot(x, jonsson_extracted['RandomDirection|COM'], '-o', linewidth=2,
                 markersize=m_size, label='RandomDirection|COM')

        ax5.set_xlim([3, 11])
        ax5.set_ylim([0, 0.55])
        ax5.set_xticks(range(3, 11), minor=True)
        ax5.xaxis.grid(which='minor')
        ax5.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax5.set_ylabel("Frequency", fontsize=axlsize)
        ax5.legend(loc=1, fontsize=11)
        fig5.tight_layout()
        plt.tick_params(axis='both', which='major', labelsize=xyticksize)
        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figureS1b.png"), format='png', dpi=600)

    if args.distribution == 'WT_comparison':
        T00 = import_to_distribution("../data/raw/2823_PA/T00_neighbours.json")
        T01 = import_to_distribution("../data/raw/2823_PA/T01_neighbours.json")
        T03 = import_to_distribution("../data/raw/2823_PA/T03_neighbours.json")
        T04 = import_to_distribution("../data/raw/2823_PA/T04_neighbours.json")
        T05 = import_to_distribution("../data/raw/2823_PA/T05_neighbours.json")
        T06 = import_to_distribution("../data/raw/2823_PA/T06_neighbours.json")
        T07 = import_to_distribution("../data/raw/2823_PA/T07_neighbours.json")
        T08 = import_to_distribution("../data/raw/2823_PA/T08_neighbours.json")
        T09 = import_to_distribution("../data/raw/2823_PA/T09_neighbours.json")
        T10 = import_to_distribution("../data/raw/2823_PA/T10_neighbours.json")

        fig6, ax6 = plt.subplots(figsize=(7.5, 6))
        x = range(0, len(T00))
        ax6.plot(x, T00, '-o', linewidth=2, markersize=m_size, label='A')
        ax6.plot(x, T01, '-o', linewidth=2, markersize=m_size, label='B')
        ax6.plot(x, T03, '-o', linewidth=2, markersize=m_size, label='C')
        ax6.plot(x, T04, '-o', linewidth=2, markersize=m_size, label='D')
        ax6.plot(x, T05, '-o', linewidth=2, markersize=m_size, label='E')
        ax6.plot(x, T06, '-o', linewidth=2, markersize=m_size, label='F')
        ax6.plot(x, T07, '-o', linewidth=2, markersize=m_size, label='G')
        ax6.plot(x, T08, '-o', linewidth=2, markersize=m_size, label='H')
        ax6.plot(x, T09, '-o', linewidth=2, markersize=m_size, label='I')
        ax6.plot(x, T10, '-o', linewidth=2, markersize=m_size, label='J')
        ax6.set_xlim(x_limits)
        ax6.set_ylim([0, 0.3])
        ax6.set_xticks(range(x_limits[0], x_limits[1]), minor=True)
        ax6.xaxis.grid(which='minor')
        ax6.set_xlabel("Number of Neighbours", fontsize=axlsize)
        ax6.set_ylabel("Frequency", fontsize=axlsize)
        ax6.legend(loc=1, fontsize=11)
        if args.outpath is not None:
            plt.savefig(os.path.join(args.outpath, "figureS9k.png"), format='png', dpi=600)

    if args.outpath is None:
        plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-o", "--outpath", help="name to save fig")
    parser.add_argument("-d", "--distribution", help="distribution to plot",
                        choices=['model_comp', 'data_comp', 'extracted_data', 'picking_behaviour',
                                 'comp_jonsson', 'comp_jonsson_all', 'WT_comparison'])
    args = parser.parse_args()
    main()
