import os
import json

import numpy as np
import matplotlib.pyplot as plt

import common_functions as ttf


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    exp_dir = '../data/raw/3002_PD'

    def get_rmsd(exp_dir, matches_tps):

        matches_filename = matches_tps + '.match'
        tp2 = matches_tps[-3:]

        matches_dict = ttf.load_matches(os.path.join(exp_dir, 'cell_matches', matches_filename))
        cell_dict_path = os.path.join(exp_dir, '%s_neighbours.json' % tp2)

        with open(cell_dict_path, "r") as input_handle:
            cell_dict = json.load(input_handle)

        rmsd = []
        for k, v in matches_dict.iteritems():
            if len(v) > 1:
                areas = [cell_dict[str(i)]['area'] for i in v]
                areas_percent = [float(i) / sum(areas) for i in areas]
                # print areas_percent, '%2f' % (areas_percent[0]-0.5)**2
                rmsd.append((areas_percent[0] - 0.5) ** 2)
        return rmsd

    def get_ratios(exp_dir, matches_tps):

        matches_filename = matches_tps + '.match'
        tp2 = matches_tps[-3:]

        matches_dict = ttf.load_matches(os.path.join(exp_dir, 'cell_matches', matches_filename))
        cell_dict_path = os.path.join(exp_dir, '%s_neighbours.json' % tp2)

        with open(cell_dict_path, "r") as input_handle:
            cell_dict = json.load(input_handle)

        ratios = []
        for k, v in matches_dict.iteritems():
            if len(v) > 1:
                areas = [float(cell_dict[str(i)]['area']) for i in v]
                # print areas
                # print areas_percent, '%2f' % (areas_percent[0]-0.5)**2
                # print max(areas) / sum(areas)
                if max(areas) / sum(areas) > 0.5:
                    ratios.append(max(areas)/(sum(areas)))
        return ratios

    rmsd_list = []
    rmsd_names = ['T00T01', 'T01T02', 'T02T03', 'T03T04', 'T04T05', 'T05T06',
                  'T06T07', 'T07T08', 'T08T09', 'T09T10', 'T10T11']
    rmsd_labelnames = ['175 to 186 HAS', '186 to 193 HAS', '193 to 199 HAS',
                       '199 to 210 HAS', '210 to 220 HAS', '220 to 233 HAS',
                       '233 to 244 HAS', '244 to 259 HAS', '259 to 287 HAS',
                       '287 to 306 HAS', '306 to 331 HAS']
    for tps in rmsd_names:
        rmsd_list.append(get_ratios(exp_dir, tps))
    rmsd_names.insert(0, '')

    fig = plt.figure()
    ax = plt.subplot(111)
    fig.suptitle('Areal asymmetry in $\it{spch}$ cell divisions')
    for i, rmsd in enumerate(rmsd_list):
        #print i
        y = rmsd
        x = np.random.normal(i + 1, 0.06, size=len(rmsd))
        for j in range(len(rmsd)):
            ax.plot(x[j], y[j], 'g.', alpha=0.5, markersize=5)

    ax.violinplot(rmsd_list)
    ax.set_xticks(range(1,len(rmsd_list)+1))
    ax.set_xticklabels(rmsd_labelnames, rotation=45,ha='right')
    #ax.set_xlabel('Timepoints')
    ax.set_ylabel('Ratio of largest daughter cell to mother cell')
    ax.set_xlim([0,len(rmsd_list)+0.5])

    outpath = "../paper/figures"
    plt.savefig(os.path.join(outpath, "figureS7.pdf"), format='pdf', bbox_inches='tight')


if __name__ == "__main__":
    main()
