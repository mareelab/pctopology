import argparse
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import os

import common_functions as cf


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    neighbours_dict = cf.load_neighbours_dictionary_json(json_path)
    cid_array = cf.path2id_array(segmented_image_path)

    del (neighbours_dict[0])

    outline_array = cf.generate_cell_outline_array(cid_array, color='black')

    upper_fraction = 0.45
    upper_pixels = upper_fraction * cid_array.shape[0]

    colour_array = np.full((cid_array.shape[0], cid_array.shape[1], 4),
                           [0, 0, 0, 1],
                           dtype=np.float32)

    upper_cids, lower_cids = [], []
    all_cids = []

    edge_cells = []

    for cid, v in neighbours_dict.iteritems():
        if 0 not in v['neighbours']:
            all_cids.append(cid)
            if v['centroid_y'] < upper_pixels:
                upper_cids.append(cid)
            elif v['centroid_y'] > upper_pixels:
                lower_cids.append(cid)
            else:
                pass
        else:
            edge_cells.append(cid)

    upper_color = [float(0) / 255, float(255) / 255, float(102) / 255, 1]
    lower_color = [float(66) / 255, float(134) / 255, float(244) / 255, 1]

    for cid in neighbours_dict.iterkeys():
        if cid in upper_cids:
            colour_array[np.where(cid_array == cid)] = upper_color
        elif cid in lower_cids:
            colour_array[np.where(cid_array == cid)] = lower_color
        elif cid in edge_cells:
            colour_array[np.where(cid_array == cid)] = [.5, .5, .5, 1]
        else:
            pass

    upper_areas = [neighbours_dict[uid]['area'] for uid in upper_cids]
    lower_areas = [neighbours_dict[lid]['area'] for lid in lower_cids]

    fig = plt.figure(figsize=(7.5,6))

    ax1 = fig.add_subplot(1,2,1)
    ax1.imshow(colour_array)
    ax1.imshow(outline_array)
    cf.add_scale_bar(ax1)

    ax2 = fig.add_subplot(1, 2, 2)
    plt.hist([upper_areas, lower_areas], color=[upper_color, lower_color],
             histtype='step', label=['upper part', 'lower part'])
    plt.subplots_adjust(left=0.05, bottom=0.09, right=0.96, top=0.95)
    plt.legend()
    string1 = "Upper, " + str.format('{0:.2f}', np.mean(upper_areas)) +\
              " +/- " + str.format('{0:.2f}', np.std(upper_areas)) + r" $\mu$m$^{2}$"
    string2 = "Lower, " + str.format('{0:.2f}', np.mean(lower_areas)) +\
              " +/- " + str.format('{0:.2f}', np.std(lower_areas)) + r" $\mu$m$^{2}$"
    ax2.text(0.95, 0.8, string1,
             verticalalignment='bottom', horizontalalignment='right',
             transform=ax2.transAxes,
             color='green', fontsize=10)
    ax2.text(0.95, 0.7, string2,
             verticalalignment='bottom', horizontalalignment='right',
             transform=ax2.transAxes,
             color='blue', fontsize=10)
    plt.xlabel(r"Cell Area, $\mu$m$^{2}$", fontsize=10)
    plt.ylabel(r"Count", fontsize=10)

    if args.outpath is not None:
        plt.savefig(os.path.join(args.outpath, "figureS2b.png"), format='png', dpi=600)
    else:
        plt.show()


def create_hist(cids, dict):
    neighbours_list = []
    for cid in cids:
        neighbours_list.append(dict[cid]['num_neighbours'])

    neighbours_hist = [0] * 18
    for i in neighbours_list:
        neighbours_hist[i] += 1

    neighbours_hist[:] = [float(x) / sum(neighbours_hist) for x in neighbours_hist]

    return neighbours_hist


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00'")
    parser.add_argument("-o", "--outpath", help="name to save fig")
    args = parser.parse_args()

    main()
