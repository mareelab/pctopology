#!/usr/bin/python
"""plots a heatmap of normalised area and number of neighbours for a leaf"""
import argparse
import os
import ast

import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.colors
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

import common_functions as ttf


def get_clim(n_list, mid_value=6):
    l_min = min(n_list)
    l_max = max(n_list)
    rng = max(l_max - mid_value, mid_value - l_min)
    return mid_value - rng, mid_value + rng


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    metadata_path = os.path.join(data_dir, args.exp_id, 'microscope_metadata', (args.time_point + '.txt'))
    md_dict = ttf.load_metadata(metadata_path)

    cid_array = ttf.path2id_array(segmented_image_path)
    neighbourhood_dictionary = ttf.load_neighbours_dictionary_json(json_path)
    del neighbourhood_dictionary[0]

    anisotropy_list = []
    cids = []

    for cid, v1 in neighbourhood_dictionary.iteritems():
        anisotropy_list.append(v1['anisotropy'])
        cids.append(cid)

    heatmap_shape = [cid_array.shape[0], cid_array.shape[1], 4]

    anisotropy = np.full(shape=heatmap_shape, fill_value=[256, 256, 256, 1], dtype='float32')

    if args.range is None:
        n_min, n_max = min(anisotropy_list), max(anisotropy_list)
    else:
        n_lim = ast.literal_eval(args.range)
        n_min, n_max = n_lim[0], n_lim[1]

    if args.colormap is None:
        color_scheme_2 = 'plasma_r'
    else:
        color_scheme_2 = str(args.colormap)

    color_map_neighbour = matplotlib.cm.ScalarMappable(cmap=color_scheme_2)
    color_map_neighbour.set_clim(vmin=n_min, vmax=n_max)

    for cid, data in neighbourhood_dictionary.iteritems():
        anisotropy[cid_array == cid] = color_map_neighbour.to_rgba(data['anisotropy'])

    cell_outlines = ttf.generate_cell_outline_array(cid_array)

    extent = [0, anisotropy.shape[1] * md_dict['vox_y'], 0, anisotropy.shape[0] * md_dict['vox_x']]

    font_size = 18

    plt.figure(facecolor='white', figsize=(7, 10))
    ax4 = plt.subplot(1, 1, 1)
    plt.title("Anisotropy", fontsize=font_size)
    im4 = plt.imshow(anisotropy, cmap=color_scheme_2, interpolation='nearest', extent=extent,
                     vmin=n_min, vmax=n_max)
    plt.imshow(cell_outlines, cmap=color_scheme_2, interpolation='nearest', extent=extent)
    plt.clim(n_min, n_max)
    ttf.add_scale_bar(ax4)
    divider4 = make_axes_locatable(ax4)
    cax4 = divider4.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im4, cax=cax4)
    cbar.ax.tick_params(labelsize=20)
    # cbar.set_label('Anisotropy')

    plt.subplots_adjust(left=0.04, bottom=0.05, right=0.96, top=0.92, wspace=0.2, hspace=0.2)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname), format='png', dpi=600)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00', 'T01' etc")
    parser.add_argument("-o", "--outname", help="name to save fig")
    parser.add_argument("-c", "--colormap", help="Heatmap colormap")
    parser.add_argument("-r", "--range", help="colormap range")
    args = parser.parse_args()

    main()
