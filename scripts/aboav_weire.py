"""Test Aboav Weire for a time point"""
import os
import argparse
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np

import common_functions as cf


def renormalise_neighbours(data_dict):
    """ calculates the average of the neighbours normed area excluding the cell of interest"""
    av_neighbours_normed_area = {}
    for cell, props_dict in data_dict.iteritems():
        temp_list2 = []
        for neighbour in props_dict['neighbours']:
            temp_list = []
            for neighbour_neighbour in data_dict[neighbour]['neighbours']:
                if neighbour_neighbour is not cell and neighbour_neighbour is not 0:
                    temp_list.append(data_dict[neighbour_neighbour]['area'])
            temp_list2.append(float(data_dict[neighbour]['area']) / (np.mean(temp_list) + 0.00000001))
        av_neighbours_normed_area[cell] = np.mean(temp_list2)
    return av_neighbours_normed_area


def main():
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    data_dir = '../data/raw'
    output_dir = '../paper/figures'
    json_path = os.path.join(data_dir, args.exp_id, (args.time_point + '_neighbours.json'))
    segmented_image_path = os.path.join(data_dir, args.exp_id, (args.time_point + '.png'))

    neighbours_dict = cf.load_neighbours_dictionary_json(json_path)
    cid_array = cf.path2id_array(segmented_image_path)

    non_edge_neighbours_dict = {k: v for k, v in neighbours_dict.iteritems() if 0 not in v['neighbours']}
    del (non_edge_neighbours_dict[0])

    """neighbour counts and histograms"""
    max_neighbours = 0
    for cid, data_dict in non_edge_neighbours_dict.iteritems():
        if len(data_dict['neighbours']) > max_neighbours:
            max_neighbours = len(data_dict['neighbours'])

    num_neighbours_list = []
    for cid, data_dict in non_edge_neighbours_dict.iteritems():
        num_neighbours_list.append(data_dict['num_neighbours'])

    num_neighbours_hist = [0] * 18
    for i in num_neighbours_list:
        num_neighbours_hist[i] += 1

    num_neighbours_hist[:] = [float(x) / sum(num_neighbours_hist) for x in num_neighbours_hist]

    x = range(0, 18)

    """average number of neighbours' neighbours"""

    av_neigh_neighbours = {}
    for cid, data_dict in non_edge_neighbours_dict.iteritems():
        nnlist = []
        for neighbour in data_dict['neighbours']:
            try:
                nnlist.append(non_edge_neighbours_dict[int(neighbour)]['num_neighbours'])
                # TODO: figure out why this is needed
            except KeyError:
                pass

        av_neigh_neighbours[cid] = sum(nnlist) / float(len(nnlist) + 0.0000000001)

    dict_by_nneighbours = defaultdict(list)
    for cid, data_dict in non_edge_neighbours_dict.iteritems():
        dict_by_nneighbours[data_dict['num_neighbours']].append(av_neigh_neighbours[cid])

    cell_neighbours = []
    cell_av_nn = []

    for k, v in av_neigh_neighbours.iteritems():
        cell_neighbours.append(non_edge_neighbours_dict[k]['num_neighbours'])
        cell_av_nn.append(v)

    x1 = []
    y1 = []
    y1_sd = []
    for k, v in dict_by_nneighbours.iteritems():
        x1.append(k)
        y1.append(np.mean(v))
        y1_sd.append(np.std(v))

    aw_law = [5 + 8 / float(i + 0.0000000001) for i in x]

    path = json_path.split('/')

    title_string = path[-2] + "_" + path[-1][:-4]

    nneighbours = []
    area = []
    norm_area = []

    data_dict = non_edge_neighbours_dict
    av_neighbour_norm_area = []
    av_neighbour_norm_area_dict = renormalise_neighbours(neighbours_dict)

    for k, v in data_dict.iteritems():
        if k is not 0:
            nneighbours.append(v['num_neighbours'])
            area.append(v['area'])
            norm_area.append(v['norm_area'])
            av_neighbour_norm_area.append(av_neighbour_norm_area_dict[k])

    metadata_path = os.path.join(data_dir, args.exp_id, "microscope_metadata", (args.time_point + '.txt'))
    metadata_dict = cf.load_metadata(os.path.join(metadata_path))

    area = [a * metadata_dict['vox_x'] * metadata_dict['vox_y'] for a in area]

    cell_dict = cf.load_neighbours_dictionary_json(json_path)
    cell_dict = {k: v for k, v in cell_dict.iteritems() if 0 not in v['neighbours']}
    del (cell_dict[0])

    total_area = 0
    cell_number = 0
    n = range(20)
    lewis = [0.25 * (a - 2) for a in n]
    total_areas_sides = [0] * 20
    total_number_sides = [0] * 20

    for cid, data in cell_dict.iteritems():
        total_areas_sides[data['num_neighbours']] += data['area']
        total_number_sides[data['num_neighbours']] += 1
        total_area += data['area']
        cell_number += 1

    average_area_sides = [float(a) / b if b is not 0 else 0 for a, b in zip(total_areas_sides, total_number_sides)]
    normalised_area_sides = [float(a) / (total_area / cell_number) for a in average_area_sides]

    path = segmented_image_path.split('/')
    font_size = 15
    labelsize = 20
    leg_fontsize = 15
    txtsize = 20

    plt.figure(1, facecolor='white', figsize=(20, 10))

    plt.subplot(2, 3, 1)
    plt.plot(x1, y1, '.', label='$\it{spch}$ Arabidopsis Leaf', color='blue')
    plt.errorbar(x1, y1, yerr=y1_sd, fmt='.', color='blue')
    plt.plot(x, aw_law, label='Aboav-Weaire Law', color='green')
    plt.ylim([5, 8])
    plt.xlim([2, 10])
    plt.legend(loc=1, fontsize=leg_fontsize)
    plt.xlabel('Number of Neighbours', fontsize=font_size)
    plt.ylabel('Average Number of Neighbours\' Neighbours', fontsize=font_size)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplot(2, 3, 3)
    x = area
    y = norm_area
    plt.plot(x, y, '.')
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)))
    plt.xlabel(r'Cell Area ($\mu$m$^2$)', fontsize=font_size)
    plt.ylabel('Normalised Area', fontsize=font_size)
    p = np.poly1d(np.polyfit(x, y, 1))
    ybar = np.sum(y) / len(y)
    ssreg = np.sum((p(x) - ybar) ** 2)
    sstot = np.sum((y - ybar) ** 2)
    Rsqr = ssreg / sstot
    plt.text(0.75 * max(x) + 0.2 * min(x), np.min(y) + 0.1 * np.max(y),
             '$R^2 = %0.2f$' % float(Rsqr), fontsize=txtsize)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplot(2, 3, 2)
    x = area
    y = nneighbours
    plt.plot(x, y, '.')
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)))
    plt.xlabel(r'Cell Area ($\mu$m$^2$)', fontsize=font_size)
    plt.ylabel('Number of Neighbours', fontsize=font_size)
    p = np.poly1d(np.polyfit(x, y, 1))
    ybar = np.sum(y) / len(y)
    ssreg = np.sum((p(x) - ybar) ** 2)
    sstot = np.sum((y - ybar) ** 2)
    Rsqr = ssreg / sstot
    plt.text(0.75 * max(x) + 0.2 * min(x), np.min(y) + 0.1 * np.max(y),
             '$R^2 = %0.2f$' % float(Rsqr), fontsize=txtsize)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplot(2, 3, 4)
    x = norm_area
    y = nneighbours
    plt.plot(x, y, '.')
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)))
    plt.xlabel('Normalised Area', fontsize=font_size)
    plt.ylabel('Number of Neighbours', fontsize=font_size)
    p = np.poly1d(np.polyfit(x, y, 1))
    ybar = np.sum(y) / len(y)
    ssreg = np.sum((p(x) - ybar) ** 2)
    sstot = np.sum((y - ybar) ** 2)
    Rsqr = ssreg / sstot
    plt.text(0.75 * max(x) + 0.2 * min(x), np.min(y) + 0.1 * np.max(y),
             '$R^2 = %0.2f$' % float(Rsqr), fontsize=txtsize)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplot(2, 3, 5)
    x = norm_area
    y = av_neighbour_norm_area
    plt.plot(x, y, '.')
    plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)))
    plt.xlabel('Normalised Area', fontsize=font_size)
    plt.ylabel('Average of Neighbours\' Normalised Area', fontsize=font_size)
    p = np.poly1d(np.polyfit(x, y, 1))
    ybar = np.sum(y) / len(y)
    ssreg = np.sum((p(x) - ybar) ** 2)
    sstot = np.sum((y - ybar) ** 2)
    Rsqr = ssreg / sstot
    plt.text(0.75 * max(x) + 0.2 * min(x), 0.8 * np.max(y) + 0.2 * np.min(y),
             '$R^2 = %0.2f$' % float(Rsqr), fontsize=txtsize)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplot(2, 3, 6)
    plt.plot(n, normalised_area_sides, '.', label="$\it{spch}$ Arabidopsis Leaf")
    plt.plot(n, lewis, label="Lewis' Law")
    plt.legend(loc=0, fontsize=leg_fontsize)
    plt.ylim([0, 2.5])
    plt.xlim([1.8, 10.2])
    plt.xlabel('Number of Neighbours', fontsize=font_size)
    plt.ylabel(r'Average Normalised Area', fontsize=font_size)
    plt.tick_params(axis='both', which='major', labelsize=labelsize)

    plt.subplots_adjust(left=0.04, bottom=0.06, right=0.96, top=0.95, wspace=0.2, hspace=0.23)

    if args.outname is None:
        plt.show()
    else:
        plt.savefig(os.path.join(output_dir, args.outname + "a.png"), format='png', dpi=600)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("exp_id", help="id of the experiment. eg. '3002_PD'")
    parser.add_argument("time_point", help="Time point in the experiment. eg. 'T00'")
    parser.add_argument("-o", "--outname", help="name to save fig")
    args = parser.parse_args()

    main()
