import os
import random
import argparse
from bisect import bisect
import math
import numpy as np
import csv
import json
from datetime import datetime

import matplotlib.pyplot as plt
from tqdm import tqdm, trange


def binomial(x, y):
    """
    x Choose y
    """
    try:
        return math.factorial(x) // math.factorial(y) // math.factorial(x - y)
    except ValueError:
        return 0


def pick_pascal(current_state, cid):
    """
    given the current state of the system and the cid of the dividing cell,
    picks the division plane according to binomial distribution
    """
    n = current_state[cid]
    cid1 = random.sample(n, 1)[0]
    ind1 = n.index(cid1)
    n_ordered = n[ind1 + 1:] + n[:ind1]
    n_ordered = n_ordered[1:-1]

    length = len(n_ordered)
    pocket = []
    for i, neighbour in enumerate(n_ordered):
        pocket.extend([neighbour] * binomial(length - 1, i))

    cid2 = random.sample(pocket, 1)[0]
    ind2 = n.index(cid2)
    index1 = min(ind1, ind2)
    index2 = max(ind1, ind2)
    return index1, index2


def pick_modified_pascal(current_state, cid):
    """
    given the current state of the system and the cid of the dividing cell,
    picks the division plane according to binomial distribution and allows 3 sided neighbours
    """
    n = current_state[cid]
    cid1 = random.sample(n, 1)[0]
    ind1 = n.index(cid1)
    n_ordered = n[ind1 + 1:] + n[:ind1]
    n_ordered = n_ordered[1:-1]

    length = len(n_ordered)
    probabilities = []

    for i in xrange(length):
        probabilities.append(binomial(length + 3, i + 2))

    probabilities = [float(i) / sum(probabilities) for i in probabilities]

    cdf = [probabilities[0]]
    for i in xrange(1, len(probabilities)):
        cdf.append(cdf[-1] + probabilities[i])

    random_ind = bisect(cdf, random.random())

    ind2 = n.index(n_ordered[random_ind])

    index1 = min(ind1, ind2)
    index2 = max(ind1, ind2)
    return index1, index2


def pick_equal(current_state, cid):
    """
    given the current state of the system and the cid of the dividing cell,
    picks the division plane by splitting the cell in half
    """
    n = current_state[cid]
    cid1 = random.sample(n, 1)[0]
    ind1 = n.index(cid1)

    if len(n) % 2 == 0:
        ind2 = (len(n) / 2 + ind1) % len(n)
    else:
        choices = [math.floor(float(len(n)) / 2), math.ceil(float(len(n)) / 2)]
        choice = random.sample(choices, 1)[0]
        ind2 = (choice + ind1) % len(n)

    index1 = min(int(ind1), int(ind2))
    index2 = max(int(ind1), int(ind2))

    return index1, index2


def synchronous(case, timesteps):
    """
    run the 'synchronous' model.
    :param case: division behaviour
    :param timesteps: number of complete division cyces to run for
    :return: model results
    """
    start_time = datetime.now()
    init_graph_1 = {
        "1": ["2", "4", "3"],
        "2": ["5", "4", "1"],
        "3": ["1", "4", "6"],
        "4": ["1", "2", "5", "8", "7", "6", "3"],
        "5": ["8", "4", "2"],
        "6": ["3", "4", "7"],
        "7": ["6", "4", "8"],
        "8": ["4", "5", "7"]
    }

    # cyclic bcs
    init_graph_2 = {
        "1": ["2", "4", "3", "7"],
        "2": ["5", "4", "1", "6"],
        "3": ["1", "4", "6", "8"],
        "4": ["1", "2", "5", "8", "7", "6", "3"],
        "5": ["8", "4", "2"],
        "6": ["3", "4", "7", "2"],
        "7": ["6", "4", "8", "1"],
        "8": ["4", "5", "3", "7"]
    }

    stats = dict()
    stats['t_mat'] = np.zeros([500, 500])
    stats['p_mat'] = np.zeros([500, 500])
    stats['p_dic'] = {}

    dynamics = [init_graph_1]
    current_state = 0
    for i in trange(timesteps):
        stats['pre_mit_neighbours'] = [0] * 1000
        stats['post_mit_neighbours'] = [0] * 1000
        current_state = dynamics[i]
        order = current_state.keys()
        random.shuffle(order)
        for cid in tqdm(order):
            ind1, ind2 = 0, 1
            n = current_state[cid]
            if case == 'no3sided':
                if len(n) > 3:
                    diff = 1
                    while diff == 1 or abs(diff) + 1 == len(n):
                        div_plane = random.sample(n, 2)
                        ind1 = min(n.index(div_plane[0]), n.index(div_plane[1]))
                        ind2 = max(n.index(div_plane[0]), n.index(div_plane[1]))
                        diff = ind2 - ind1
                else:
                    pass
            elif case == '3sided':
                div_plane = random.sample(n, 2)
                ind1 = min(n.index(div_plane[0]), n.index(div_plane[1]))
                ind2 = max(n.index(div_plane[0]), n.index(div_plane[1]))

            elif case == 'pascal':
                if len(n) > 3:
                    ind1, ind2 = pick_pascal(current_state, cid)
                else:
                    pass
            elif case == 'equal_div':
                ind1, ind2 = pick_equal(current_state, cid)
            else:
                pass
            current_state, stats = divide_cell(current_state, cid, ind1, ind2, stats)

        pre_mit_neighbours = [float(j) / sum(stats['pre_mit_neighbours']) for j in stats['pre_mit_neighbours']]
        post_mit_neighbours = [float(k) / sum(stats['post_mit_neighbours']) for k in stats['post_mit_neighbours']]

        dynamics.append(current_state)

    time = datetime.now() - start_time

    for i in range(4, 200 - 4):
        if sum(stats['t_mat'][i]) != 0:
            stats['t_mat'][i] *= (2 ** (i - 4) / sum(stats['t_mat'][i]))
            # except ZeroDivisionError or RuntimeWarning:
            #    pass

    np.set_printoptions(precision=2, suppress=True)

    sum_p = sum(sum(stats['p_mat']))

    for i in range(200):
        if sum(stats['p_mat'][i]) != 0:
            stats['p_mat'][i] /= sum(stats['p_mat'][i])
            stats['p_mat'][i] *= 100

    percentages = np.zeros_like(stats['p_mat'])
    for i in range(200):
        percentages[:, i] = stats['p_mat'][:, i] * i

    sum_percent = sum(sum(percentages))

    for i in range(20):
        percentages[i, :] = sum(percentages[i, :]) / 100 * (sum_p / sum_percent)
        # print sum(percentages[i, :]) / 100 * (sum_p / sum_percent)

    print timesteps, "\nrounds of divisions took", time, "to complete"
    model_res = plot_distribution(current_state, visualise=False)

    return model_res, current_state


def aboav_weire(current_state):
    """
    plot the aboav weire relationship of the given state
    """

    number_of_neighbours = []
    number_of_neighbours_neighbours = []

    for cell, neighbours in current_state.iteritems():
        number_of_neighbours.append(len(neighbours))
        neighbour_sizes = []
        for neighbour in neighbours:
            neighbour_sizes.append(len(current_state[neighbour]))
        # print neighbour_sizes
        # print float(sum(neighbour_sizes))/len(neighbour_sizes)
        number_of_neighbours_neighbours.append(float(sum(neighbour_sizes)) / len(neighbour_sizes))

    plt.plot(number_of_neighbours, number_of_neighbours_neighbours, '.')
    plt.show()


def asynchronous(case, total_divisions):
    """
    run the asynchronous version of the model
    :param case: division behaviour
    :param total_divisions: total number of cell divisions
    :return:
    """
    start_time = datetime.now()
    init_graph_1 = {
        "1": ["2", "4", "3"],
        "2": ["5", "4", "1"],
        "3": ["1", "4", "6"],
        "4": ["1", "2", "5", "8", "7", "6", "3"],
        "5": ["8", "4", "2"],
        "6": ["3", "4", "7"],
        "7": ["6", "4", "8"],
        "8": ["4", "5", "7"]
    }

    stats = dict()

    stats['t_mat'] = np.zeros([200, 200])
    stats['p_mat'] = np.zeros([200, 200])
    stats['p_dic'] = {}

    stats['pre_mit_neighbours'] = [0] * 1000
    stats['post_mit_neighbours'] = [0] * 1000

    current_state = init_graph_1
    for _ in tqdm(range(total_divisions)):
        cid = random.choice(current_state.keys())
        n = current_state[cid]
        ind1, ind2 = 0, 0
        if case == 'no3sided':
            if len(n) > 3:
                diff = 1
                while diff == 1 or abs(diff) + 1 == len(n):
                    div_plane = random.sample(n, 2)
                    ind1 = min(n.index(div_plane[0]), n.index(div_plane[1]))
                    ind2 = max(n.index(div_plane[0]), n.index(div_plane[1]))
                    diff = ind2 - ind1
            else:
                pass
        elif case == '3sided':
            div_plane = random.sample(n, 2)
            ind1 = min(n.index(div_plane[0]), n.index(div_plane[1]))
            ind2 = max(n.index(div_plane[0]), n.index(div_plane[1]))
        elif case == 'pascal':
            if len(n) > 3:
                ind1, ind2 = pick_pascal(current_state, cid)
            else:
                pass
        elif case == 'equal_div':
            ind1, ind2 = pick_equal(current_state, cid)
        else:
            pass
        current_state, stats = divide_cell(current_state, cid, ind1, ind2, stats)

    time = datetime.now() - start_time

    pre_mit_neighbours = [float(j) / sum(stats['pre_mit_neighbours']) for j in stats['pre_mit_neighbours']]
    post_mit_neighbours = [float(k) / sum(stats['post_mit_neighbours']) for k in stats['post_mit_neighbours']]

    model_res = plot_distribution(current_state, visualise=False)

    print total_divisions, " divisions took", time, "to complete"

    return model_res, current_state


def divide_cell(current_state, cid, ind1, ind2, stats):
    n = current_state[cid]
    div_plane_1 = n[ind1]
    div_plane_2 = n[ind2]

    row = len(n)

    stats['pre_mit_neighbours'][row] += 1

    if cid in stats['p_dic']:
        stats['p_mat'][stats['p_dic'][cid][0], stats['p_dic'][cid][1]] += 1
        del stats['p_dic'][cid]

    subset1 = n[ind1:ind2]
    subset1.append(n[ind2])

    subset2 = n[ind2:] + n[:ind1]
    subset2.append(n[ind1])

    n_cell_1 = cid + "a"
    n_cell_2 = cid + "b"

    del (current_state[cid])

    for cid_r in subset1[1:-1]:
        check_list = current_state[cid_r]
        current_state[cid_r] = [n_cell_2 if i == cid else i for i in check_list]

    for cid_r in subset2[1:-1]:
        check_list = current_state[cid_r]
        current_state[cid_r] = [n_cell_1 if i == cid else i for i in check_list]

    if div_plane_1 in stats['p_dic']:
        stats['p_dic'][div_plane_1][1] += 1

    if div_plane_2 in stats['p_dic']:
        stats['p_dic'][div_plane_2][1] += 1

    pos1 = current_state[div_plane_1].index(cid)
    current_state[div_plane_1][pos1:pos1 + 1] = (n_cell_2, n_cell_1)

    pos2 = current_state[div_plane_2].index(cid)
    current_state[div_plane_2][pos2:pos2 + 1] = (n_cell_1, n_cell_2)

    current_state[n_cell_1] = subset2
    current_state[n_cell_1].append(n_cell_2)

    current_state[n_cell_2] = subset1
    current_state[n_cell_2].append(n_cell_1)

    c1 = len(current_state[n_cell_1])
    c2 = len(current_state[n_cell_2])

    stats['p_dic'][n_cell_1] = [c1, 0]
    stats['p_dic'][n_cell_2] = [c2, 0]

    stats['post_mit_neighbours'][c1] += 1
    stats['post_mit_neighbours'][c2] += 1

    stats['t_mat'][row, c1] += 1
    stats['t_mat'][row, c2] += 1

    return current_state, stats


def export_csv(state):
    path = os.path.dirname(os.path.realpath(__file__))
    file_name = os.path.join(path, "model_output.csv")
    with open(file_name, 'wb') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        for k, v in state.iteritems():
            row = [k]
            for nid in v:
                row.append(nid)
            writer.writerow(row)


def plot_distribution(state, visualise=True):
    hist = [0] * 200
    x = range(0, 200)
    for neighbours in state.itervalues():
        hist[len(neighbours)] += 1
    norm_hist = [float(i) / sum(hist) for i in hist]
    model_values = [0] * 199
    model_values[5:9] = [0.2888, 0.4640, 0.2085, 0.0359, 0.0028]
    if visualise:
        plt.plot(x, model_values)
        plt.plot(x, norm_hist)
        plt.xlabel('Number of Neighbours')
        plt.ylabel('Frequency')
        plt.xlim([0, 25])
        plt.show()

    return norm_hist


def export_results(result, out_path):
    result_dict = {}
    for n, freq in enumerate(result):
        result_dict[n] = freq

    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    with open(out_path, 'w') as output_handle:
        json.dump(result_dict, output_handle)


def export_neighbours(neigh, out_path):
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    with open(out_path, 'w') as output_handle:
        json.dump(neigh, output_handle)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--model_type", "-m", help="model type",
                        choices=['equal_div', 'pascal', '3sided', 'no3sided'], default='equal_div')
    parser.add_argument("--timing_type", "-t", help="division timing behaviour",
                        choices=['synchronous', 'asynchronous'], default='synchronous')
    parser.add_argument("--output_dir", "-o", help="Output directory for distribution results")
    parser.add_argument("--output_dir2", "-o2", help="Output directory for neighbourhood results")
    args = parser.parse_args()

    random.seed(0)
    divs = 85000
    div_rounds = 15

    if args.timing_type == 'synchronous':
        results, neigh = synchronous(str(args.model_type), div_rounds)
    elif args.timing_type == 'asynchronous':
        results, neigh = asynchronous(str(args.model_type), divs)
    else:
        results, neigh = None, None

    if args.output_dir is not None:
        export_results(results, args.output_dir)

    if args.output_dir2 is not None:
        export_neighbours(neigh, args.output_dir2)
