#!/usr/bin/env bash
# run on directory containing data directories
# extracts both junction and neighbour dictionaries for each experiment


for d in ${1}/* ; do
    if [[ ${d} == *.png ]] && [[ ${d} != *"image"* ]];
    then
        echo ${d}
        python extract_junction_dictionary.py ${d}
        python extract_neighbour_dictionary.py ${d}
    fi
done

