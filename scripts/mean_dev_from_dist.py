import json
import argparse


def main():
    with open(args.json_file) as fh:
        dist = json.load(fh)

    p_6 = 0
    t = 0
    mean = []
    mean_s = []

    if "0" in dist.keys():
        del(dist["0"])

    for i, j in dist.iteritems():
        if type(j) is not list:
            if 0 not in j["neighbours"]:
                mean.append(float(j["num_neighbours"]))
                mean_s.append(float(j["num_neighbours"])**2)
                if int(j['num_neighbours']) == 6:
                    p_6 += 1
            t += 1
        else:
            mean.append(float(len(j)))
            mean_s.append(float(len(j))**2)
            if len(j) == 6:
                p_6 += 1
            t += 1

    mu_1 = sum(mean)/float(len(mean))
    mu_2 = sum(mean_s)/float(len(mean)) - (sum(mean)/float(len(mean)))**2

    print "mu_1: ", mu_1
    print "mu_2: ", mu_2
    print "mu_2/mu_1:", mu_2/mu_1
    print "p_6: ", p_6/float(t)
    print "a: ", mu_2 * (p_6/float(t))**2

    return 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("json_file")

    args = parser.parse_args()
    main()
